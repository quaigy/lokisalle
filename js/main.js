$(document).ready(function(){

	$('#date_arrivee').datetimepicker({
	    format: 'yyyy-mm-dd hh:ii',
	    maxView: 3,
	    language: 'fr',
	    minuteStep: 10,
	    disabledHours: [8,22],
	    startDate: new Date()
	});

	$('#date_depart').datetimepicker({
	    format: 'yyyy-mm-dd hh:ii',
	    maxView: 3,
	    language: 'fr',
	    minuteStep: 10,
	    disabledHours: [8,22],
	    startDate: new Date()
	});

});