$('document').ready(function(){

	/*
	 * JS pour index.php
	 */ 

	// affiche le prix séléctionné
	// on utilise 'input' pour activer le jquery quand on bouge le slider
	$('#prix').on('input', function(){ 
		$('#displayprix').html($(this).val() + ' €'); 
	});

	// Toutes les parties du formulaire activent la requete Ajax
	
	// Les list-group deviennent actif quand on clic dessus
	$('.categorie button').click(function () {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			classer();
		} else {
			$('.categorie button').removeClass('active');
			$(this).addClass('active');
			classer();
		}
	});

	$('.ville button').click(function () {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			classer();
		} else {
			$('.ville button').removeClass('active');
			$(this).addClass('active');
			classer();
		}
	});

	// La capacité de la salle execute la requete à la selection d'un prix
	$('#capacite').on('change', function(){
		classer(); 
		
	});

	// le prix execute la requete quand on relache le clic sur le slider
	// (pour cela on utilise 'change' plutot que 'input')
	$('#prix').on('change', function(){ 
		console.log($(this).val());
		classer();
	});

	$('#date_arrivee').on('change',function(){
		classer();
	});	

	$('#date_depart').on('change',function(){
		classer();
	});


	// la requete Ajax proprement dite
	function classer(){
		var categorie = $('.categorie').find('.active').attr('data-categorie');
		var ville = $('.ville').find('.active').attr('data-ville');
		var capacite = $('#capacite').val();
		var date_arrivee = $('#date_arrivee').val();
		var date_depart = $('#date_depart').val();
		if ($('#prix').val() === '0'){ // on force une variable vide pour le traitement PHP
			var prix = '';
		} else {
			var prix = $('#prix').val();
		}
		
		$.ajax('index-classer.php',{
			method : 'POST',
			data : {
				categorie : categorie,
				ville : ville,
				capacite : capacite,
				prix : prix,
				date_arrivee : date_arrivee,
				date_depart : date_depart
			},
			dataType : 'JSON',
			success : function(data){
				$('#resultats').html('');
				if (data === 'failure'){
					$('#resultats').html('<legend>Oh non :(</legend><p>Pas de résultats correspondant à vos critères de recherche.</p>');
					$('#displayCount').html('').append('<small>Pas de résultats.</small>');
				} else {
					if (data.length == 1){
						var nbResultats = '1 résultat.';
					} else {
						var nbResultats = data.length + ' résultats.'
					}
					$('#resultats').append('<div class="row" id="produits">')
					$('#displayCount').html('').append('<small>'+ nbResultats +'</small>');
					$.each(data,function(p, product){
						$('#resultats #produits').append('<div class="col-md-4"><div class="thumbnail">' +
						'<img src="'+product.photo+'" class="thumbnail-edit mb-10" />'+
						'<p class="strong">Salle '+product.titre+ '<span class="pull-right">'+
						product.prix+' €</span></p>'+
						'<p class="text-justify">'+product.description+'</p>'+
						'<p><span class="glyphicon glyphicon-calendar"></span> Du '+product.date_arrivee+
						' au '+product.date_depart+'</p>'+
						'&nbsp;'+product.note+'<span class="pull-right"><a href="'+product.fiche+'"><span '+
						'class="glyphicon glyphicon-search" title="Voir la fiche produit">&nbsp;</span>'+
						'Voir</a></span></div>'+
						'</div>');
					});
					$('#resultats').append('</div>');
				}
				
			}
		});
	}

});