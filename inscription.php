<?php
require 'include/init.php';

$errors = [];
$pseudo = $mdp = $nom = $prenom = $email = $civilite = '';
/**
 *  Si on active le formulaire ..
 */
if (!empty($_POST)){

	/**
	 * Preparation du traitement
	 * (sanitize + extraction des variables du $_POST)
	 */
	
	sanitizePost();
	extract($_POST);

	/**
	 * Gestion des erreurs sur chaque champ du $_POST.
	 * Contraintes SQL : Pseudo et email doivent être uniques.
	 */
	
	if (empty($pseudo)){
		$errors['pseudo'] = 'Un pseudonyme est obligatoire.';
	} elseif (strlen($pseudo) < 6){
		$errors['pseudo'] = 'Le pseudo doit contenir plus de 6 caractères.';
	} elseif (strlen($pseudo) > 20){
		$errors['pseudo'] = 'Le pseudo doit contenir moins de 20 caractères.';
	} elseif ( !preg_match('/^[a-zA-Z0-9_-]{6,20}$/',$pseudo)) {
		$errors['pseudo'] = 'Caractères acceptés : chiffres, lettres, - et _ .';
	} else {
		$query = "SELECT COUNT(*) FROM membre WHERE pseudo = :pseudo";
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
		$stmt->execute();
		if ($stmt->fetchColumn() == 1){
			$errors['pseudo'] = 'Ce pseudo existe déjà. Merci d\'en choisir un autre.';
		}
	}

	if (empty($mdp)){
		$errors['mdp'] = 'Un mot de passe est obligatoire.';
	} elseif (strlen($mdp) < 6){
		$errors['mdp'] = 'Votre mot de passe doit faire plus de 6 caractères de long.';
	} elseif ($mdp == $pseudo){
		$errors['mdp'] = 'Votre mot de passe doit être différent de votre pseudonyme.';
	} elseif ($mdp == $email){
		$errors['mdp'] = 'Votre mot de passe doit être différent de votre email.';
	}

	if (empty($nom)){
		$errors['nom'] = 'Votre nom est obligatoire.';
	} elseif (strlen($nom) > 20){
		$errors['nom'] = 'Votre nom doit faire moins de 20 caractères.';
	} 

	if (empty($prenom)){
		$errors['prenom'] = 'Votre prénom est obligatoire.';
	} elseif (strlen($prenom) > 20){
		$errors['prenom'] = 'Votre prénom doit faire moins de 20 caractères.';
	} 

	if (empty($email)){
		$errors['email'] = 'Votre email est obligatoire';
	} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$errors['email'] = 'Format d\'email invalide.';
	} else {
		$query = "SELECT COUNT(*) FROM membre WHERE email = :email";
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->execute();
		if ($stmt->fetchColumn() == 1){
			$errors['email'] = 'Cet email existe déjà. Merci d\' en utiliser un autre.';
		}
	}

	/**
	 * Pas d'erreurs durant l'inscription ?
	 * Alors on crypte le mot de passe et on enregistre
	 * toutes les informations dans la base de données.
	 */
	
	if (empty($errors)){
		$md5 = md5($mdp);

		$query = 'INSERT INTO membre (pseudo, mdp, nom, prenom, email, civilite) '
				. 'VALUES (:pseudo, :mdp, :nom, :prenom, :email, :civilite)'
				;
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
		$stmt->bindParam(':mdp', $md5, PDO::PARAM_STR);
		$stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
		$stmt->bindParam(':prenom', $prenom, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->bindParam(':civilite', $civilite, PDO::PARAM_STR);
		$stmt->execute();

		setFlashMessage('Vous êtes maintenant inscrit');
		header('Location: connexion.php');
		die();
	} else {
		setFlashMessage('Erreur(s) dans le formulaire', 'error');
	}
}

require 'layout/header.php';
require 'layout/nav.php';
?>

<div class="container">

		<fieldset>
		<?php 
		displayFlashMessage();
		?>
		<legend>Inscription</legend>
		<form method="post">

			<div class="form-group <?= getErrorClass('pseudo', $errors); ?>">
				<input type="text" class="form-control" name="pseudo" placeholder="Votre pseudo" value="<?= $pseudo; ?>" />
				<?= displayErrorMsg('pseudo', $errors) ?>
			</div>

			<div class="form-group <?= getErrorClass('mdp', $errors); ?>">
				<input type="password" class="form-control" name="mdp" placeholder="Votre mot de passe" />
				<?= displayErrorMsg('mdp', $errors) ?>
			</div>

			<div class="form-group <?= getErrorClass('nom', $errors); ?>">
					<input type="text" class="form-control" name="nom" placeholder="Votre nom" value="<?= $nom; ?>" />
				<?= displayErrorMsg('nom', $errors) ?>
			</div>

			<div class="form-group <?= getErrorClass('prenom', $errors); ?>">
				<input type="text" class="form-control" name="prenom" placeholder="Votre prénom" value="<?= $prenom; ?>" />
				<?= displayErrorMsg('prenom', $errors) ?>
			</div>

			<div class="form-group <?= getErrorClass('email', $errors); ?>">
				<input type="email" class="form-control" name="email" placeholder="Votre email" value="<?= $email; ?>" />
				<?= displayErrorMsg('email', $errors) ?>
			</div>

			<div class="form-group <?= getErrorClass('civilite', $errors); ?>">
				<select name="civilite" class="form-control">
					<option value="m" <?php ('m' === $civilite) ? ' selected="selected"' : ''; ?>>Homme</option>
					<option value="f" <?php ('f' === $civilite) ? ' selected="selected"' : ''; ?>>Femme</option>
				</select>
			</div>

			<div class="form-group">
				<button class="btn btn-primary">Inscription</button>
			</div>


		</form>

		</fieldset>

</div><!-- /.container -->
<?php require 'layout/footer.php'; ?>
