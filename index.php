<?php
require 'include/init.php';
$loadTimePicker = '';
$loadAjax = '';
require 'layout/header.php';
require 'layout/nav.php';



/**
* On sélectionne tout les produits à afficher
* LEFT JOIN sur les avis pour afficher les salles n'ayant pas encore de notes
* On n'affiche pas les produits antérieurs à la date du jour ($now)
*/
$now = date("Y-m-d H:i:s", time());
$query = "SELECT p.*, s.titre, s.photo, s.description, a.note" 
					. " FROM produit p"
					. " JOIN salle s" 
					. " USING (id_salle)"
					. " LEFT JOIN avis a" // LEFT JOIN au cas ou une salle n'ai pas de note
					. " USING (id_salle)"
					. " WHERE p.date_arrivee > ". $pdo->quote($now)
					. " AND p.etat != 'reservation'"
					. " ORDER BY p.date_arrivee"
					;


$stmt = $pdo->query($query);
$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
$countProduits = count($produits);
/**
 * Requêtes utilisées pour le <select> et le <input range> du classement
 */
$stmt = $pdo->query("SELECT capacite FROM salle GROUP BY capacite ORDER BY capacite ASC");
$selects = $stmt->fetchAll(PDO::FETCH_ASSOC);

$stmt = $pdo->query("SELECT MIN(prix) as max FROM produit WHERE date_arrivee >" . $pdo->quote($now) . " AND etat = 'libre'");
$prixMin = $stmt->fetchColumn();

$stmt = $pdo->query("SELECT MAX(prix) as max FROM produit WHERE date_arrivee >" . $pdo->quote($now) . " AND etat = 'libre'");
$prixMax = $stmt->fetchColumn();
?>

<div class="container">
<?= displayFlashMessage() ?>
	<div class="row">

		<!-- Critères de recherche-->
		<div class="col-md-2">
			<div class="form-group">
				<label for="categorie">Catégorie</label>
				<div class="list-group categorie">
					<button type="button" class="list-group-item" data-categorie="reunion">Réunion</button>
					<button type="button" class="list-group-item" data-categorie="bureau">Bureau</button>
					<button type="button" class="list-group-item" data-categorie="formation">Formation</button>
				</div>
			</div>

			<div class="form-group">
				<label for="villes">Ville</label>
				<div class="list-group ville">
					<button type="button" class="list-group-item" data-ville="Paris">Paris</button>
					<button type="button" class="list-group-item" data-ville="Lyon">Lyon</button>
					<button type="button" class="list-group-item" data-ville="Marseille">Marseille</button>
				</div>
			</div>

			<div class="form-group">
				<label for="capacite">Capacité max </label>
				<select id="capacite" class="form-control">
					<option value="">Peu importe</option>
					<?php
					foreach ($selects as $option) :
					?>
					<option value="<?= $option['capacite'] ?>"><?= $option['capacite'] ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>

			<div class="form-group">
				<label>Prix max : </label>&nbsp;<span id="displayprix">0 €</span>
				<input type="range" class="form-control" id="prix" value="0" min="0" max="<?= $prixMax?>" step ="100"/>
			</div>

			<div class="form-group">
			<label for="periode" class="control-label">Période</label><br>
				<small>Date d'arrivée</small>
				<div class="input-group input-group-sm">
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					<input type="text" id="date_arrivee" class="form-control" name="date_arrivee" placeholder="YYYY-mm-dd hh" />
				</div>
				
				<br>
				<small>Date de départ</small>
				<div class="input-group input-group-sm">
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					<input type="text" id="date_depart" class="form-control" name="date_depart" placeholder="YYYY-mm-dd hh" />
				</div>
				

				<div class="row">
					<br>
					<div id="displayCount" class="col-md-8 col-md-offset-2 thumbnail text-center"><small><?= $countProduits ? $countProduits . ' résultats.' : '' ?> </small>
				</div>
				</div>

			</div>
		</div>

		<!-- Liste des produits-->
		<div class="col-md-9 col-md-offset-1" id="resultats">
			<div class="row">
				<?php
				foreach ($produits as $produit) :
				?>
				<div class="col-md-4">
					<div class="thumbnail">
					<img src="<?= PHOTO_SITE . $produit['photo'] ?>" class="thumbnail-edit mb-10" />
					<p><strong>Salle <?= $produit['titre'] ?></strong><span class="pull-right"><?= $produit['prix'] ?> €</span></p>
					<p class="text-justify"><?= substr($produit['description'], 0, 54) . '...' ?></p>
					<p><span class="glyphicon glyphicon-calendar"></span> Du
					<?= date("d/m/Y", (strtotime($produit['date_arrivee']))) . ' au ' .
					date("d/m/Y", (strtotime($produit['date_depart']))) ;?>
					</p>
					&nbsp;<?= afficherEtoiles($produit['note']) ?>
					<span class="pull-right"><a href="<?= RACINE_SITE . 'produit-view.php?id='. $produit['id_produit'] ?>"><span class="glyphicon glyphicon-search" title="Voir la fiche produit">&nbsp;</span>Voir</a></span>
					</div>
				</div>
				<?php
				endforeach;
				?>
			</div>
		</div>
	</div>
</div>
<?php
require 'layout/footer.php';