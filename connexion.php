<?php
require 'include/init.php';
require 'layout/header.php';

$errors = [];
$pseudo = $mdp = '';

/**
 *  Si on active le formulaire ..
 */
if (!empty($_POST)){

	/**
	 * Preparation du traitement
	 * (sanitize + extraction des variables du $_POST)
	 */
	sanitizePost();
	extract($_POST);

	if (empty($pseudo)){
		$errors['pseudo'] = 'Entrez votre pseudo pour vous connecter.';
	}

	if (empty($mdp)){
		$errors['mdp'] = 'Entrez votre mot de passe pour vous connecter.';
	}

	/**
	 * Pas d'erreurs dans le formulaire ?
	 * On crypte le mot de passe puis on vérifie si le couple pseudo/mdp
	 * correspond avec un utilisateur dans la BDD
	 */
	if (empty($errors)){
		$mdp = md5($mdp);

		$query = "SELECT * FROM membre WHERE pseudo = :pseudo AND mdp = :mdp";
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
		$stmt->bindParam(':mdp', $mdp, PDO::PARAM_STR);
		$stmt->execute();
		$login = $stmt->fetch(PDO::FETCH_ASSOC);

		/**
		 *  Si on obtient pas de résultat, on renvoie une erreur
		 *  Sinon, on enleve le mdp des infos, on remplace la civilité telle
		 *  qu'exprimée dans la base de données par une version plus lisible 
		 *  puis on enregistre le tout dans la $_SESSION avant de rediriger 
		 *  l'utilisateur vers l'index
		 */
		
		if (empty($login)){
			setFlashMessage('Identifiants incorrects.', 'error');
		} else {
			unset($login['mdp']);
			if ($login['civilite'] == 'm'){
				$login['civilite'] = 'Monsieur';
			} else {
				$login['civilite'] = 'Madame';
			}
			$_SESSION['user_info'] = $login;
			setFlashMessage('Vous êtes maintenant connecté.');
			header('Location: index.php');
			die();

		}
	} else {
		setFlashMessage('Erreur(s) dans le formulaire', 'error');
	}
}
require 'layout/nav.php';
?>
<div class="container">

	<div class="starter-template">
		<fieldset>
		<?php 
		displayFlashMessage();
		?>
		<legend>Connexion</legend>
			<form method="post">

				<div class="form-group <?= getErrorClass('pseudo', $errors); ?>">
					<input type="text" class="form-control" name="pseudo" placeholder="Votre pseudo" value="<?= $pseudo; ?>" />
					<?= displayErrorMsg('pseudo', $errors) ?>
				</div>

				<div class="form-group <?= getErrorClass('mdp', $errors); ?>">
					<input type="password" class="form-control" name="mdp" placeholder="Votre mot de passe" />
					<?= displayErrorMsg('mdp', $errors) ?>
				</div>

				<div class="form-group">
					<button class="btn btn-primary">Se connecter</button>
				</div>

			</form>

		</fieldset>
	</div>

</div><!-- /.container -->


<?php
require 'layout/footer.php';
?>