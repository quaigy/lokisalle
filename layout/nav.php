<?php 
if (isUserAdmin()) : 
?>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<a class="navbar-brand" href="<?= RACINE_SITE; ?>admin/index.php">Admin</a>

		<ul class="nav navbar-nav">
			<li>
				<a href="<?= RACINE_SITE; ?>admin/salles.php">Gestion des salles</a>
			</li>
			<li>
				<a href="<?= RACINE_SITE; ?>admin/produits.php">Gestion produits</a>
			</li>
			<li>
				<a href="<?= RACINE_SITE; ?>admin/commandes.php">Gestion commandes</a>
			</li>
			<li>
				<a href="<?= RACINE_SITE; ?>admin/avis.php">Gestion avis</a>
			</li>			
			<li>
				<a href="<?= RACINE_SITE; ?>admin/membres.php">Gestion membres</a>
			</li>
		</ul>
		</div>
	</nav>
<?php
endif;
?>

	<nav class="navbar navbar-default">
		<div class="container">
			<!-- Menu hamburger - responsive BS -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Menu</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= RACINE_SITE; ?>index.php">Lokisalle</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?= RACINE_SITE; ?>qui.php">Qui Sommes Nous</a></li>
					<li><a href="<?= RACINE_SITE; ?>contact.php">Contact</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<?php
						if (isConnected()) :
						?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?= $_SESSION['user_info']['pseudo'] ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?= RACINE_SITE; ?>profil.php"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;&nbsp;Votre profil</a></li>
							<li><a href="<?= RACINE_SITE; ?>deconnexion.php"><span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;&nbsp;Déconnexion</a></li>
						</ul>

						<?php
						else :
						?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Espace Membres <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?= RACINE_SITE; ?>inscription.php">Inscription</a></li>
							<li><a href="<?= RACINE_SITE; ?>connexion.php">Connexion</a></li>
						</ul>
						<?php
						endif;
						?>

					</li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>