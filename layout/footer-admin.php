
	<!-- jQuery-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Bootstrap JS-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php
if (isset($loadTimePicker)) :
?>
	<!-- Timepicker + traduction fr -->
	<script src="<?= RACINE_SITE; ?>js/bootstrap-datetimepicker.js" type="text/javascript"></script>
	<script src="<?= RACINE_SITE; ?>js/bootstrap-datetimepicker.fr.js" type="text/javascript" charset="UTF-8"></script>
	<!-- Appel Timepicker -->
	<script src="<?= RACINE_SITE; ?>js/main.js"></script>
<?php
endif;
?>
	</body>
</html>