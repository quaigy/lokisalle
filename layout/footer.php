	<footer class="footer">
		<br><br>
		<p class="text-center"><a href="ml.php" data-toggle="modal" data-target="#ML">Mentions légales</a> - <a href="cvg.php" data-toggle="modal" data-target="#CVG">Conditions générales de vente</a></p>
		<p class="text-center">Site réalisé dans le cadre d'une formation <a href="http://www.ifocop.fr/formations-metiers/web-digital/formation-developpeur-php-oriente-objet/">IFOCOP</a>. Lokisalle - 2017</p>
		<!-- modals -->
		<div class="modal fade" id="ML" tabindex="-1" role="dialog" aria-labelledby="ML">
		<div class="modal-dialog" role="document">
    		<div class="modal-content">
    		</div>
  		</div>
		</div>

		<div class="modal fade" id="CVG" tabindex="-1" role="dialog" aria-labelledby="CVG">
		<div class="modal-dialog" role="document">
    		<div class="modal-content">
    		</div>
  		</div>
		</div>
	</footer>


	<!-- jQuery-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Bootstrap JS-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<?php
if (isset($loadAjax)) :
?>
	<!-- Ajax pour la home -->
	<script src="<?= RACINE_SITE; ?>js/ajax.js" type="text/javascript"></script>
<?php
endif;
?>

<?php
if (isset($loadTimePicker)) :
?>
	<!-- Timepicker + traduction fr -->
	<script src="<?= RACINE_SITE; ?>js/bootstrap-datetimepicker.js" type="text/javascript"></script>
	<script src="<?= RACINE_SITE; ?>js/bootstrap-datetimepicker.fr.js" type="text/javascript" charset="UTF-8"></script>
	<!-- Appel Timepicker -->
	<script src="<?= RACINE_SITE; ?>js/main.js"></script>
<?php
endif;
?>
	</body>
</html>