<?php
require 'include/init.php';
require 'layout/header.php';
/**
* Affichage des infos sur le produit et la salle en fonction de l'id passée en GET
*/
if (!empty($_GET) && isset($_GET['id'])){
	$query = "SELECT p.*, s.* FROM produit p JOIN salle s ON p.id_salle = s.id_salle WHERE p.id_produit = ". $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$produit = $stmt->fetch(PDO::FETCH_ASSOC);
	/**
	* ID ne correspond à aucun produit -> redirect
	*/
	if (empty($produit)){
		header('Location: index.php');
		die();
	}
} else {
	/**
	* Pas de GET ou d'id -> redirect
	*/
	header('Location: index.php');
	die();
}
/**
* Affichage aléatoire d'autrs produits
*/
$produits = selectRandomProducts($pdo, $_GET['id']);

/**
* Affichage des avis et de la note
*/
$query = "SELECT a.*, m.pseudo FROM avis a"
		." JOIN membre m ON a.id_membre = m.id_membre"
		." WHERE id_salle = " . $pdo->quote($produit['id_salle'] 
		." ORDER BY a.date_enregistrement");
$stmt = $pdo->query($query);
$avisMembres = $stmt->fetchAll(PDO::FETCH_ASSOC);

$adresse = preg_replace('/\s+/', '_', $produit['adresse']);
require 'layout/nav.php';
?>
<div class="container">
	<?= displayFlashMessage() ?>
<fieldset>
	<div class="row">
		<h2><?= $produit['titre'] ?>&nbsp;<?= afficherEtoilesMoyenne($avisMembres) ?>
		<?php 
		if (isConnected() && ($produit['etat'] != 'reservation')) :
		?>
		<span class="pull-right"><a class="btn btn-primary btn-sm" href="produit-commande.php?id=<?= $produit['id_produit'] ?>">
		Réserver</a></span>
		<?php
		elseif (isConnected()) :
		?>
		<span class="pull-right"><h5>Ce produit n'est pas disponible.</h5></span>
		<?php
		else :
		?>
		<span class="pull-right"><h5><a href="inscription.php">Inscrivez-vous</a> ou <a href="connexion.php">connectez-vous</a> pour réserver ce produit</h5></span>
		<?php
		endif;
		?>
		</h2>
		<hr>
	</div>

	<div class="row">
		<div class="col-md-8">
			<img src="<?= PHOTO_SITE . $produit['photo'] ?>">
		</div>
		<div class="col-md-4">
			<p><strong>Description</strong></p>
			<p><?= $produit['description'] ?></p>
			<p><strong>Localisation</strong></p>
			<iframe width="300" height="230" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBb3Y0li-40tpU0to2oPHAkhnSM7zU5gf0&q=<?= $adresse ?>,<?= $produit['cp'] ?>+<?= $produit['ville']?>" allowfullscreen></iframe>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<p><strong>Informations complémentaires</strong></p>
		</div>
		<div class="col-md-4">
			<p><span class="glyphicon glyphicon-calendar">&nbsp;</span>Arrivée : <?= date("d/m/Y", (strtotime($produit['date_arrivee']))) ?></p>
			<p><span class="glyphicon glyphicon-calendar">&nbsp;</span>Départ : <?= date("d/m/Y", (strtotime($produit['date_depart']))) ?></p>
		</div>		
		<div class="col-md-4">
			<p><span class="glyphicon glyphicon-user">&nbsp;</span>Capacité : <?= $produit['capacite'] ?></p>
			<p><span class="glyphicon glyphicon-inbox">&nbsp;</span>Catégorie : <?= $produit['categorie'] ?></p>
		</div>		
		<div class="col-md-4">
			<p><span class="glyphicon glyphicon-map-marker">&nbsp;</span>Adresse : <?= $produit['adresse'] .', '. $produit['cp'] . ', ' . $produit['ville'] ?></p>
			<p><span class="glyphicon glyphicon-eur">&nbsp;</span>Tarif : <?= $produit['prix'] ?> €</p>
		</div>
	</div>
	<br>
		<!-- Commentaires -->
	<div class="row">
	<legend>Avis de nos membres</legend>
	<?php
	if (empty($avisMembres)) :
	?>
	<div class="row">
		<div class="col-md-6">
		<p>Pas encores d'avis sur cette salle :(.</p>
		</div>
	</div>
	<?php
	else :
	?>
		<?php
		foreach ($avisMembres as $avisMembre) :
		?>
		<div class="row">
			<div class="col-md-2">
			<p><?= $avisMembre['pseudo'] ?><br><h6><?= formatDateJMA($avisMembre['date_enregistrement']) ?></h6></p>
			</div>
			<div class="col-md-1">
			<?= afficherEtoiles($avisMembre['note']) ?>	
			</div>
			<div class="col-md-7">
			<?= $avisMembre['commentaire'] ?>
			</div>
		</div>
		<br>
		<?php
		endforeach;
		?>
	
	<?php
	endif;
	?>
	</div>
	<!-- Afficher une sélection aléatoire d'autres produits disponibles -->
	<div class="row">
	<legend>Autres produits</legend>
		<?php
		for ($i = 0; $i < 4; $i++) :
		?>
		<div class="col-md-3">
			<div class="thumbnail">
			<a href="produit-view.php?id=<?= $produits[$i]['id_produit'] ?>">
			<img src="<?= PHOTO_SITE . $produits[$i]['photo'] ?>" class="thumbnail-delete"></a>
			</div>
		</div>
		<?php
		endfor;
		?>

	</div>


	<div class="col-md-12">
		<a href="salle-avis.php?id=<?= $produit['id_salle'] ?>&produit=<?= $produit['id_produit'] ?>">Déposer un commentaire et une note</a><span class="pull-right"><a href="index.php">Retour au catalogue</a></span>
	</div>
</fieldset>
</div>
<?php
require '/layout/footer.php'
?>
