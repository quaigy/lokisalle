<?php
require 'include/init.php';
require 'layout/header.php';
require 'layout/nav.php';
?>

	<div class="container">
	<div class="col-md-4">
		<div class="login-panel panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Connexion</h3>
			</div>
			<div class="panel-body">
				<form role="form" name="login" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Identifiant" name="identifiant" type="text" autofocus="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="javascript:;" class="btn btn-sm btn-success">Login</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
		</div>

	</div><!-- /.container -->

<?php
require '/layout/footer.php'
?>