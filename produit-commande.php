<?php
require 'include/init.php';
require 'layout/header.php';

if (empty($_GET)){
	redirectMsg('Vous ne pouvez pas accéder à cette page', 'error');
} else {
	if (isConnected()){

		if (!empty($_GET)){
			$query = "SELECT p.date_arrivee, p.date_depart, p.prix, s.titre, s.adresse, s.cp, s.ville "
					. "FROM produit p "
					. "JOIN salle s ON p.id_salle = s.id_salle "
					. "WHERE p.id_produit = ". $pdo->quote($_GET['id'])
					;
			$stmt = $pdo->query($query);
			$commande = $stmt->fetch(PDO::FETCH_ASSOC);

			if (!$commande) {
				redirectMsg('Produit inconnu','error');
			} 
		} 

		if (isset($_POST['valider-commande'])){

			$query="INSERT INTO commande (id_membre, id_produit, date_enregistrement)"
							. " VALUES (:id_membre, :id_produit, :date_enregistrement)"
							;
			$date = date("Y-m-d H:i:s", time());
			$stmt = $pdo->prepare($query);
			$stmt->bindParam(':id_membre', $_SESSION['user_info']['id_membre'], PDO::PARAM_INT);
			$stmt->bindParam(':id_produit', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindParam(':date_enregistrement', $date, PDO::PARAM_STR);
			$stmt->execute();

			$stmt = $pdo->query("UPDATE produit "
								. "SET etat = 'reservation' "
								. "WHERE id_produit = ". $pdo->quote($_GET['id']))
								;
			$stmt->execute();
			redirectMsg('Commande pour le produit #'. $_GET['id']. ' passée avec succès.');
		}

		if (isset($_POST['annuler-commande'])){
			header('Location: index.php');
			die();
		}
	} else {
		/**
		 * Tentative d'accéder à cette page sans être connecté ? Retour à l'index.
		 */
		header('Location: index.php');
		die();
	}
}


require 'layout/nav.php';
?>

<div class="container">
	<fieldset>
		
		<div class="row">
			<div class="col-md-8">
				<legend>Résumé de la commande</legend>
				<table class="table table-striped table-bordered table-condensed">
					<tr>
						<th class="text-center inverse">Infos salle</th>
						<th class="text-center inverse">Période</th>
						<th class="text-center inverse">Tarif</th>
					</tr>
					<tr>
						<td class="text-center">
							<p>Salle <?= $commande['titre']?></p>
							<p><?= $commande['adresse'] . ', ' . $commande['cp'] . ', ' . $commande['ville'] ?></p>
						</td>
						<td class="text-center">
							<p>Du <?= date("d/m/Y h:m", (strtotime($commande['date_arrivee']))) ?></p>
							<p>Au <?= date("d/m/Y h:m", (strtotime($commande['date_depart']))) ?></p>

						</td>
						<td class="text-center"><br><?= $commande['prix'] ?> €</td>
					</tr>
				</table>
			</div>
			<div class="col-md-4">
			<legend>Validation</legend>
				<div class="thumbnail">
					<button class="btn btn-primary btn-block" name="annuler-commande">Annuler</a><br>
					<form method="post">
						<button class="btn btn-success btn-block" name="valider-commande">Valider la commande</button>
					</form>
				</div>
			</div>
		</div>
	</fieldset>
</div>

<?php
require 'layout/footer.php';
?>