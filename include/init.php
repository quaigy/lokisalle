<?php
session_start();
/*setlocale(LC_TIME, 'fr_FR.utf8','fra');*/
header('content-type: text/html; charset=utf-8');

/*define pour ifocop*/
// define('RACINE_WEB', '/php/lokisalle/');
// define('RACINE_SITE', $_SERVER['DOCUMENT_ROOT'] . '/php/lokisalle/');

/*define pour maison*/
define('RACINE_WEB', '/perso/lokisalle/');
define('RACINE_SITE', $_SERVER['DOCUMENT_ROOT'] . '/htdocs/lokisalle/');
define('PHOTO_WEB', RACINE_WEB . 'img/');
define('PHOTO_SITE', RACINE_SITE . 'img/');


require __DIR__ . '/connexion.php';
require __DIR__ . '/fonctions.php';