<?php

		//////////////////////////
		// SANITIZE USER INPUTS //
		//////////////////////////

/**
 * Supprime les whitespace et le code HTML/PHP
 * dans une variable auto référencée
 * @param  string &$value [ Variable d'entrée ]
 * @return string         [ Retourne la variable en référence ]
 */
function sanitizeValue(&$value) {
	$value = trim(strip_tags($value));
}

/**
 * Parcours un tableau et utilise sanitizeValue()
 * sur chaque élément
 * @param  array  &$array [ Tableau d'entrée ]
 * @return array          [ Retourne le tableau en référence ]
 */
function sanitizeArray(array &$array){
	array_walk($array, 'sanitizeValue');

}
/**
 * Sanitize $_POST
 * @return 					[ Renvoie une version propre de $_POST ]
 */
function sanitizePost(){
	sanitizeArray($_POST);
}

		////////////////////////////
		// FORMS ERRORS & DISPLAY //
		////////////////////////////

/**
 * Ajouter un message d'erreur spécifique en cas d'erreur
 * dans un formulaire.
 * @param  string $inputname [ Nom du champ ]
 * @param  array  $errors  	 [ Le tableau des erreurs du formulaire ]
 * @return string            [ Le message d'erreur mis en forme si une erreur
 *                             est présente pour le champ, sinon une chaine vide ]
 */
function displayErrorMsg($inputname, array $errors) {
	if (isset($errors[$inputname])) {
		return '<div class="help-block">&nbsp;&nbsp;<span class="glyphicon glyphicon-warning-sign aria-hidden="true">&nbsp;</span><strong>Erreur !</strong>&nbsp;' . $errors[$inputname] . '</div>';
	} else {
		return '';
	}
}

/**
 * Si un message d'erreur existe pour le champs,
 * rajoute une class 'has-error'
 * @param  string $inputname  [ Nom du champ ]
 * @param  array  $errors     [ Le tableau des erreurs du formulaire ]
 * @return string             [ La class 'has-error' ou une chaine vide ]
 */
function getErrorClass($inputname, array $errors) {
	if (isset($errors[$inputname])) {
		return ' has-error';
	} else {
		return '';
	}

}

		////////////////////////////////
		// FLASH MESSAGES 			  //
		// (Erreurs,succès et autres) //
		////////////////////////////////

/**
 * Enregistre dans $_SESSION un message à afficher ensuite
 * @param string $message [ Le message à afficher ]
 * @param string $type    [ Optionnel : le type de message.
 *                          Par défaut, sera 'success']
 */
function setFlashMessage($message, $type = "success") {
	$_SESSION['flashMessage'] = [
		'message' => $message,
		'type' => $type,
	];
}

/**
 * Afficher un message flash enregistré dans $_SESSION
 * avec des variations selon le type de message
 * puis effacer le message dans $_SESSION.
 * @return  [ Un simple echo avec du HTML à la sauce Bootstrap ]
 */
function displayFlashMessage(){
	if (isset($_SESSION['flashMessage'])){
		$type = $_SESSION['flashMessage']['type'];

		if ($type == 'error') {
			$type = 'danger';
			$glyphicon = 'glyphicon-exclamation-sign';
		} elseif ($type == 'warning'){
			$glyphicon = 'glyphicon-off';
		} else {
			$glyphicon = 'glyphicon-thumbs-up';
		}

		echo '<div class="alert alert-'. $type . ' alert-dismissible"'
		. ' role="alert"><button type="button" class="close" data-dismiss="alert" '
		. 'aria-label="Close"><span aria-hidden="true">&times;</span></button>'
		. '<span class="glyphicon '. $glyphicon .' aria-hidden="true">&nbsp;</span>'
		. '<strong>' . $_SESSION['flashMessage']['message'] . '</strong></div>';
		unset($_SESSION['flashMessage']);
	}
}

		//////////////////////////
		// GESTION USER & ADMIN //
		//////////////////////////

/**
 * On assume qu'un utilisateur est connecté 
 * si $_SESSION['user_infos'] existe
 * @return boolean [ true = connecté ]
 */
function isConnected(){
	return isset($_SESSION['user_info']);
}

/**
 * Est-ce que l'utilisateur a le statut admin ?
 * @return boolean [true = admin]
 */
function isUserAdmin(){
	return isConnected() && $_SESSION['user_info']['statut'] == 1;
}

function adminSecurity(){
	if (!isUserAdmin()){
		if(!isConnected()) {
			header('Location: ' . RACINE_SITE . 'connexion.php');
		} else {
			header('Location: ' . RACINE_SITE . 'index.php');
		}
		die();
	}
}

		////////////////////
		// FORMATAGE DATE //
		////////////////////

/**
 * Affiche une date en toutes lettres au format français
 * Type : 12 Janvier 2017
 * Construction hasardeuse à partir de plusieurs sources des Internets
 * Le if{} n'existe que pour passer outre un problème avec 
 * un serveur qui tourne sous Windows.
 * @param  [int] $input [Une date (assumée au format SQL)]
 * @return [str]        [Date complète en français]
 */
function formatDateJMA($input){
	$input = strtotime($input);
	if ($_SERVER['SERVER_NAME'] == "website.com"){ // Online
		$nb_jour= "%e"; // Number of the day without "0"
	} else {
		$nb_jour = "%#d";  // The same on Windows/Wampserver
	}
/*	setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');*/
	setlocale(LC_TIME, 'fra'); 
	return  utf8_encode(ucfirst(strftime('%A '.$nb_jour.' %B %Y', $input)));
}

		/////////////////////////
		// PRODUITS ALEATOIRES //
		/////////////////////////

/**
 * Une selection de produits ne contenant pas l'id du produit affichée
 * ni les produits antérieurs à la date du jour.
 * On shuffle() le résultat pour un affichage aléatoire sur la page
 * produit-view.php
 * @param  objet  $pdo        [Un objet PDO]
 * @param  int    $id_product [l'id du produit à ne pas afficher]
 * @return array          	  [un tableau contenant les produits]
 */
function selectRandomProducts(PDO $pdo, $id_product){
	$stmt = $pdo->query("SELECT p.id_produit, s.photo FROM produit p "
			."JOIN salle s ON p.id_salle = s.id_salle "
			."WHERE p.etat = 'libre' "
			."AND p.date_arrivee > NOW()"
			."AND p.id_produit != "
			. $pdo->quote($id_product));
	
	$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
	shuffle($produits);

	return $produits;
}

		/////////////////////
		// AFFICHAGE NOTES //
		/////////////////////

function moyenneNote(array $notes) {
	if (!$notes){ // on veut éviter les divisions par zéro
		return '';
	} else {
		$moyenne = '';
		foreach ($notes as $note){
			$moyenne += $note['note'];
		}
		$moyenne = ceil($moyenne / count($notes));
	}
	return ($moyenne);
}

function afficherEtoiles($input){
	$etoiles = '';
	for ($i = 0; $i < $input; $i++){
		$etoiles .= '<span class="glyphicon glyphicon-star star"></span>';
	}
	return $etoiles;
}

function afficherEtoilesMoyenne($note){
	return afficherEtoiles(moyenneNote($note));
}

		//////////////////////
		// ORDER BY TABLEAU //
		//////////////////////
/**
 * Prend un tableau associatif en entrée et fabrique un morceau de query
 * SQL avec. 
 * Pas une fonction parfaite mais j'en suis quand même fier.
 */
function getOrderBy(array $results) {
	$query = '';

	if(isset($_GET['classer'])){
		$query .= 'ORDER BY ';
		foreach($results as $id => $table){
			if ($_GET['classer'] == $id){
				$query .= $table[0] . '.' . $id ;
			} 
		}
		$query .= ' ' . $_GET['ordre'] . ' ';
	} else {
		$table = reset($results);
		$query .= 'ORDER BY ' . $table[0]. '.' . key($results) . ' ASC ';
	}
	return $query;
}

		////////////////
		// PAGINATION //
		////////////////


function pagination($pdo, $table, $parPage){
	// On compte le nombre de choses à afficher
	$query = "SELECT COUNT(*) AS total FROM " . $table;
	$stmt = $pdo->query($query);
	$nbProduits = $stmt->fetchColumn();
	// On en déduit le nombre de pages 
	$nbPages = ceil($nbProduits/$parPage);

		if (isset($_GET['page'])){
		// On récupère le numéro de la page dans le GET
			$pageActuelle = intval($_GET['page']);
		// Si le n° est plus grand que le maximum, on remplace $pageActuelle par le max
			if ($pageActuelle > $nbPages){
				$pageActuelle = $nbPages;
			}
		// Si pas de $_GET['page'], la page actuelle est la première
		} else {
			$pageActuelle = 1;
		}

	$debutPagination = ($pageActuelle-1)*$parPage;
	// On triche en renvoyant un tableau contenant les 3 variables
	// nécessaires à l'execution du reste du code sur la page
	return array($nbPages,$pageActuelle, $debutPagination);
}

function redirectMsg($message, $type = "success", $location=RACINE_SITE ) {
	setFlashMessage($message, $type);
	header('Location: ' .$location);
	die();
}