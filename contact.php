<?php
require 'include/init.php';
require 'layout/header.php';
require 'layout/nav.php';
// TODO : intégration d'un envoi de mail en PHP
?>
<div class="container">
	<fieldset>
		<legend>Contactez-nous</legend>
		<div class="col-md-7 col-md-offset-3 thumbnail">
			<img src="<?= PHOTO_SITE ?>team.jpg" class="pt-20" alt="Notre équipe" title="Notre équipe de winners.">
			<hr>
			<blockquote class="text-center ml-50 mr-50" style="border-right : solid 5px #eee">
  			<h3>Lokisalle Paris</h3>
  			<small>37/39 rue Saint Sébastien</small>
  			<p>75011 Paris</p>
  			<p><span class="glyphicon glyphicon-phone"></span>09 66 66 66 66</p>
  			<p><span class="glyphicon glyphicon-mail"></span><a href="#">dummy@email.com</a></p>
			</blockquote>
			<br>

		</div>
	</fieldset>
</div>
<?php
require '/layout/footer.php';
?>