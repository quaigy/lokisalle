# Lokisalle
Projet réalisé dans le cadre d'une formation Developpeur PHP à IFOCOP Paris XI.


Site pour une entreprise fictive proposant à la location des salles de réunions, formations et bureau.

Réalisé entièrement en PHP procédural (sauf pour PDO, il ne faut pas exagérer)

Back office : 
- Gestion des salles, produits, membres, commandes et avis

Front :
- Inscription, affichage des produits, ajout de notes et commentaires

# Installation

/include
-- connexion.php
<?php

$pdoOptions = [
	PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
];

$pdo = new PDO('mysql:host=localhost;dbname=', '','', $pdoOptions);

Rajouter son nom de BDD et les infos de connexions à mySQL

# Javascript

Utilise Bootstrap et le plugin Bootstrap-DateTimePicker 

# Contact
quaigy@gmail.com