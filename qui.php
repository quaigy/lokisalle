<?php
require 'include/init.php';
require 'layout/header.php';
require 'layout/nav.php';
?>
<div class="container">
	<fieldset>
		<legend>Qui sommes nous ?</legend>
		<div class="col-md-7 col-md-offset-3 thumbnail">
			<img src="<?= PHOTO_SITE ?>team.jpg" class="pt-20" alt="Notre équipe" title="Notre équipe de winners.">
			<hr>
			<h3 class="text-center">Une équipe dynamique</h3>
			<p class="text-justify ml-10 mr-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquam, purus eget tempus suscipit, nulla libero pharetra lectus, 
			vel blandit magna nisl nec massa. Sed quis ante ut arcu placerat dapibus. Aenean vel risus metus. Fusce maximus varius tincidunt.
			 Sed venenatis interdum quam eu mollis. Nulla eget nibh accumsan, ultricies sapien quis, feugiat tellus. Nam egestas non lacus nec tempus. 
			 In in lobortis quam, tristique faucibus metus. In hac habitasse platea dictumst. Nulla mattis purus sit amet varius hendrerit.</p>
			<p class="text-justify ml-10 mr-10">Sed ultrices blandit mi bibendum venenatis. Suspendisse venenatis felis placerat aliquam porttitor. Nulla suscipit faucibus mauris, 
			eget elementum mi vestibulum ac. Phasellus fermentum turpis id turpis sollicitudin blandit. Nullam dapibus ante ac lacus malesuada malesuada. 
			Mauris nibh turpis, convallis ut diam quis, facilisis cursus libero. Suspendisse a vehicula eros.</p>
			<br>

			<blockquote class="text-center ml-30 mr-30" style="border-right : solid 5px #eee">
  			<p><strong>❝</strong> Peut importe de faire du Front nul tant que le Back fonctionne.<strong>❞</strong></p>
			<footer>Kévin B. dans <cite title="devoo13">DEVOO13</cite></footer>
			</blockquote>

			<h3 class="text-center">Un service innovant</h3>
			<p class="text-justify ml-10 mr-10">Morbi tincidunt diam sed lorem fermentum gravida ac a dolor. Pellentesque hendrerit massa eu bibendum vehicula. Aliquam efficitur 
			vitae orci vitae consequat. Integer quis suscipit sem. Ut vestibulum aliquam scelerisque. Fusce sollicitudin dolor in sodales fringilla. Nulla rhoncus 
			convallis leo, at porttitor elit auctor venenatis. Vivamus convallis velit non ultricies dignissim. Sed et tortor a eros consectetur posuere. Curabitur 
			tortor ipsum, pharetra non enim a, blandit auctor nibh.</p>
			<p class="text-justify ml-10 mr-10">Ut imperdiet nibh nisi, et eleifend felis ornare eget. Quisque varius bibendum ullamcorper. Fusce in rhoncus orci. Vestibulum 
			scelerisque ullamcorper ante et vestibulum. Nullam ultrices dolor lacus, eu cursus sem porta tincidunt. Nullam eu feugiat lacus, at fringilla ex. 
			Praesent vestibulum lobortis vestibulum.</p>
			<br>

			<blockquote class="text-center ml-30 mr-30" style="border-right : solid 5px #eee">
  			<p><strong>❝</strong>Bon, ok, faut quand même du Front potable malgré tout.<strong>❞</strong></p>
			<footer>Kévin B. dans <cite title="devoo13">DEVOO13</cite></footer>
			</blockquote>

			<h3 class="text-center">Une vision tournée vers l'avenir</h3>
			<p class="text-justify ml-10 mr-10">Donec semper facilisis ipsum blandit suscipit. Praesent ullamcorper enim mi, a ultricies velit mattis quis. Proin condimentum 
				nisi iaculis, consectetur turpis ac, lobortis odio. Etiam rutrum aliquet turpis vel venenatis. In blandit consectetur venenatis. Integer sagittis 
				nisi nec mi dignissim, a rhoncus turpis posuere. Fusce congue mi laoreet mattis faucibus. Nullam pharetra urna id euismod tristique. Vivamus ac 
				dolor nec nisl tincidunt lobortis. Quisque vel metus metus. Sed quis odio in est consectetur eleifend at in libero. Ut finibus massa eu 
				lobortis eleifend. Aliquam id malesuada massa. Quisque non tempus erat, non laoreet velit. In vestibulum dignissim molestie. Mauris efficitur 
				eget velit eget auctor.</p>
			<br>
			<h3 class="text-center">Ils parlent de nous</h3>
			<div class="row">
				<div class="col-md-4">
					<img class="thumbnail img-responsive" src="<?= PHOTO_SITE ?>journal1.jpg" title=".. meilleur service de locations pour vos réunions B2B.">
				</div>				
				<div class="col-md-4">
					<img class="thumbnail img-responsive" src="<?= PHOTO_SITE ?>journal2.jpg" title=".. rachetez les propriétés quand ils fermeront !">
				</div>				
				<div class="col-md-4">
					<img class="thumbnail img-responsive" src="<?= PHOTO_SITE ?>journal3.jpg" title=".. C'est vrai ça, pourquoi pas vous ?">
				</div>
			</div>
		</div>
	</fieldset>
</div>
<?php
require '/layout/footer.php';
?>