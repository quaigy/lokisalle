<?php
require 'include/init.php';
if (!empty($_POST)){
	$now = date("Y-m-d H:i:s", time());
	$query = "SELECT p.*, s.titre, s.photo, s.description, a.note" 
					. " FROM produit p"
					. " JOIN salle s" 
					. " USING (id_salle)"
					. " LEFT JOIN avis a" // LEFT JOIN au cas ou une salle n'ai pas de note
					. " USING (id_salle)"
					. " WHERE p.etat != 'reservation'"
					. " AND p.date_arrivee > ". $pdo->quote($now)
					;

	if (!empty($_POST['categorie'])){
		$query .= " AND s.categorie = " . $pdo->quote($_POST['categorie']);
	}
	if (!empty($_POST['ville'])){
		$query .= " AND s.ville = " . $pdo->quote($_POST['ville']);
	}	
	if (!empty($_POST['capacite'])){
		$query .= " AND s.capacite <= " . $_POST['capacite'];
	}
	if (!empty($_POST['prix'])){
		$query .= " AND p.prix <=" . $_POST['prix'];
	}
	if (!empty($_POST['date_arrivee'])){
		$query .= " AND p.date_arrivee >= " . $pdo->quote($_POST['date_arrivee']);
	}
	if (!empty($_POST['date_depart'])){
		$query .= " AND p.date_depart >= " . $pdo->quote($_POST['date_depart']);
	}
	$query .= " GROUP BY p.id_produit ORDER BY p.date_arrivee";
	$stmt = $pdo->query($query);
	$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($produits){
		/**
		 * On traite les données recues de la base SQL
		 * on ajoute aussi l'url pour la fiche produit
		 */
		$compteur = count($produits);
		for ($i = 0; $i < $compteur; $i++){
			$produits[$i]['photo'] = PHOTO_SITE . $produits[$i]['photo'];
			$produits[$i]['description'] = substr($produits[$i]['description'], 0, 54) . '...';
			$produits[$i]['date_arrivee'] = date("d/m/Y", (strtotime($produits[$i]['date_arrivee'])));
			$produits[$i]['date_depart'] = date("d/m/Y", (strtotime($produits[$i]['date_depart'])));
			$produits[$i]['note'] = afficherEtoiles($produits[$i]['note']);
			$produits[$i]['fiche'] = RACINE_SITE . 'produit-view.php?id=' . $produits[$i]['id_produit'];
		}
		echo json_encode($produits, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
	} else {
		echo json_encode('failure', JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
	}
}

	/**
	* Irrelevant stuff. 
 	* Filtre les entrées vides du tableau
 	*/
/*	$sortItems = array_filter($_POST);
	foreach($sortItems as $colonne => $value){
		if ($colonne == 'categorie'){
			$query .= " AND s.categorie = " . $pdo->quote($value);
		} elseif ($colonne == 'ville'){
			$query .= " AND s.ville = " . $pdo->quote($value);
		} elseif ($colonne == 'capacite'){
			$query .= " AND s.capacite = " . $value;
		} elseif ($colonne == 'prix'){
			$query .= " AND p.prix <=" . $value;
		} elseif ($colonne == 'date_arrivee'){
			$query .= " AND p.date_arrivee >= " . $pdo->quote($value);
		} else {
			$query .= " AND p.date_depart >= " . $pdo->quote($value);
		}
	}*/

?>