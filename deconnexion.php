<?php
require 'include/init.php';
/**
 * Deconnecter l'utilisateur en utilisant unset sur $_SESSION['user_info']
 * puis au moyen de $_SERVER['HTTP_REFERER'] recuperer la page ou se trouvait
 * l'utilisateur afin de l'y rediriger. 
 * Par défaut, renvoyer sur l'index.
 */
unset($_SESSION['user_info']);
$redirect = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php');
setFlashMessage('Vous êtes maintenant déconnecté', 'warning');
header('Location: '. $redirect);
die();