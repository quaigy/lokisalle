<?php

require 'include/init.php';
require 'layout/header.php';
/**
 * Page uniquement accessible si l'on est connecté
 * Toute tentative d'y rentrer directement renvoie 
 * l'utilisateur vers la page de connexion.
 */
if (!isConnected()) {
	header('Location: connexion.php');
	die();
} else {
	$query = "SELECT c.*, p.date_arrivee, p.date_depart, p.prix, s.titre, s.adresse, s.cp, s.ville, s.capacite "
			. "FROM commande c "
			. "JOIN produit p ON c.id_produit = p.id_produit "
			. "JOIN salle s ON p.id_salle = s.id_salle "
			. "WHERE c.id_membre = " . $pdo->quote($_SESSION['user_info']['id_membre'])
			. "ORDER BY c.date_enregistrement"
			;
	$stmt = $pdo->query($query);
	$commandes = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$errors = [];
$pseudo = $email = $mdp = '';
/**
 * Traitement du formulaire
 */
if (!empty($_POST)){
	/**
	 * Preparation du traitement
	 * (sanitize + extraction des variables du $_POST)
	 */
	sanitizePost();
	extract($_POST);
	/**
	 * Si l'utilisateur n'entre pas de pseudo, on récupère celui de la session.
	 */
	if (empty($pseudo)){
		$pseudo = $_SESSION['user_info']['pseudo'];
		echo 'Pseudo vide, pseudo de session passé en parametre';
	/**
	 * Si le pseudo du POST diffère de celui de la SESSION, on teste la validité
	 * du pseudo et s'il est déjà présent dans la base de données.
	 */
	} elseif (!empty($pseudo) && $pseudo != $_SESSION['user_info']['pseudo']){
		echo 'Pseudo non vide, pseudo different de la session';
		if (strlen($pseudo) < 6){
			$errors['pseudo'] = 'Le pseudo doit contenir plus de 6 caractères.';
		} elseif (strlen($pseudo) > 20){
			$errors['pseudo'] = 'Le pseudo doit contenir moins de 20 caractères.';
		} elseif ( !preg_match('/^[a-zA-Z0-9_-]{6,20}$/',$pseudo)){
			$errors['pseudo'] = 'Caractères acceptés : chiffres, lettres, - et _ .';
		} else {
			$query = "SELECT COUNT(*) FROM membre WHERE pseudo = :pseudo";
			$stmt = $pdo->prepare($query);
			$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
			$stmt->execute();
			if ($stmt->fetchColumn() == 1){
				$errors['pseudo'] = 'Ce pseudo existe déjà. Merci d\'en choisir un autre.';
			}
		}
	} 
	/**
	 * Si l'utilisateur n'entre pas d'email, on récupère celui de la session.
	 */
	if (empty($email)){
		$email = $_SESSION['user_info']['email'];
		echo 'email vide, email de session passé en paramètre';
	/**
	 * Si l'email du POST diffère de la session, on teste la validité de l'email
	 * et la contrainte d'email unique dans la base de données.
	 */
	} elseif (!empty($email) && $email != $_SESSION['user_info']['email']){
		echo 'email non vide, différent de la session';
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Format d\'email invalide.';
		} else {
			$query = "SELECT COUNT(*) FROM membre WHERE email = :email";
			$stmt = $pdo->prepare($query);
			$stmt->bindParam(':email', $email, PDO::PARAM_STR);
			$stmt->execute();
			if ($stmt->fetchColumn() == 1){
				$errors['email'] = 'Cet email existe déjà. Merci d\' en utiliser un autre.';
			}
		}
	}
	/**
	 * Si l'utilisateur entre un nouveau mot de passe, on effectue des tests 
	 * simples pour augmenter la sécurité.
	 */
	if (!empty($mdp)){
		if (strlen($mdp) < 6) {
			$errors['mdp'] = 'Le mot de passe doit faire plus de 6 caractères.';
		} elseif ($mdp == $pseudo) {
			$errors['mdp'] = 'Le mot de passe doit être différent du pseudo.';
		} elseif ($mdp == $email) {
			$errors['mdp'] = 'Le mot de passe doit être différent de l\'email.';
		}
	}
	/** 
	 * Pas d'erreurs dans le POST, on prepare la mise à jour de 
	 * la base avec les nouvelles infos. On ne changera le mot de passe
	 * que si l'utilisateur en rentre un dans le formulaire.
	 */
	if (empty($errors)) {
		$query = 'UPDATE membre SET'
					. ' pseudo = :pseudo,'
					. ' email = :email'
					;
		if (!empty($mdp)){
			$md5 = md5($mdp);
			$query .= ', mdp = :mdp';
		}

		$query .= ' WHERE id_membre = :id';
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		if (!empty ($mdp)){
			$stmt->bindParam(':mdp', $md5, PDO::PARAM_STR);
		}
		$stmt->bindParam(':id', $_SESSION['user_info']['id_membre'], PDO::PARAM_INT);
		$stmt->execute();
	/**
	 * Base mise à jour, on met maintenant à jour la SESSION
	 * On unset le mdp du POST pour ne pas l'ajouter en clair par erreur
	 * dans notre SESSION. 
	 */
		unset($_POST['mdp']); // gtfo empty quotes, too.
		$_SESSION['user_info']['pseudo'] =  $pseudo;
		$_SESSION['user_info']['email'] =  $email;
		if (!empty($_POST)){
			setFlashMessage('Informations modifiées avec succès.');
		}
	}
} 

/**
 * Une date plus lisible pour l'utilisateur.
 * Utilisé dans l'affichage avec la fonction
 * formatDateJMA() 
 */
$date = formatDateJMA($_SESSION['user_info']['date_enregistrement']);

require 'layout/nav.php';
?>

<div class="container">
	<div class="row">
		<?php 
		displayFlashMessage();
		?>
		<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title-lg"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;Informations</h3>
			</div>
			<div class="panel-body">
				<p><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;
					<?= $_SESSION['user_info']['civilite']. ' ' . $_SESSION['user_info']['prenom'] . ' ' .$_SESSION['user_info']['nom']; ?></p>
				<p><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<?= $_SESSION['user_info']['email']; ?></p>
				<p><span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;Membre depuis le : <?= $date ?></p>
				<hr>
				<fieldset>
				<legend>Modifier vos informations</legend>
					<form method="post">
						<div class="form-group <?= getErrorClass('pseudo', $errors); ?>">
							<input type="text" class="form-control " name="pseudo" placeholder="Votre nouveau pseudo" value="<?= $pseudo; ?>" />
							<?= displayErrorMsg('pseudo', $errors) ?>
						</div>
						<div class="form-group <?= getErrorClass('mdp', $errors); ?>">
							<input type="password" class="form-control " name="mdp" placeholder="Votre nouveau mdp" />
							<?= displayErrorMsg('mdp', $errors) ?>
						</div>
						<div class="form-group <?= getErrorClass('email', $errors); ?>">
							<input type="email" class="form-control " name="email" placeholder="Votre nouvel email" value="<?= $email; ?>" />
							<?= displayErrorMsg('email', $errors) ?>
						</div>

						<button class="btn btn-primary align-right">Modifier</button>
					</form>
				</fieldset>
			</div>
		</div>
		</div>

		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title-lg"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;Historique des commandes.</h3>
				</div>
				<div class="panel-body" id="table">
				<?php
				if (!empty($commandes)) :
				?>
					<table class="table table-striped table-bordered table-condensed">
						<tr>
							<th class="text-center inverse">Date commande</th>
							<th class="text-center inverse">Salle</th>
							<th class="text-center inverse">Dates</th>
							<th class="text-center inverse">Tarif</th>
						</tr>
						<?php
						foreach ($commandes as $commande) :
						$dateCommande = formatDateJMA($commande['date_enregistrement']);
						?>
						<tr class="text-center">
							<td><?= $dateCommande ?></td>
							<td><p><strong><?= 'Salle ' . $commande['titre'] ?></strong>
								<a href="produit-view.php?id=<?= $commande['id_produit'] ?>"><span class="glyphicon glyphicon-search pull-right"></span></a></p>
								<p><?= $commande['adresse']?>,<br> <?= $commande['cp'] . ', ' . $commande['ville'] ?></p>
								<p><span class="glyphicon glyphicon-user">&nbsp;</span><?= $commande['capacite'] ?> personnes</p>
							</td>
							<td>Du : <?= date("d/m/Y \à H:i", (strtotime($commande['date_arrivee']))); ?><br>
								Au : <?= date("d/m/Y \à H:i", (strtotime($commande['date_arrivee']))); ?></td>
							<td><?= $commande['prix'] ?> €</td>
						</tr>
						<?php
						endforeach;
						?>
					</table>
				<?php
				else : 
				?>
					<p>Pas encore de commandes</p>
				<?php 
				endif;
				?>
				</div>
			</div>
		</div>

	</div> <!-- row -->

</div> <!-- container -->

<?php					
require 'layout/footer.php';