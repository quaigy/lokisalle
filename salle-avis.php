<?php
require 'include/init.php';
require 'layout/header.php';
/**
* On ne veut que des utilisateurs connectés.
*/ 
if (!isConnected()){
    header('Location: connexion.php');
}
/*
* Il faut impérativement un ?id= pour afficher la page
*/
if (!isset($_GET['id'])){
    header('Location: index.php');
} else {
    /**
    * On vérifie que l'utilisateur à déjà loué cette salle avant de lui laisser
    * ajouter un commentaire. 
    * En bonus : si l'id de la salle est inexistante, la redirection s'éxecute aussi.
    */
    $stmt = $pdo->query("SELECT * FROM commande c JOIN produit p ON c.id_produit = p.id_produit"
                        ." WHERE p.id_salle = " . $pdo->quote($_GET['id'])
                        ." AND c.id_membre = " . $pdo->quote($_SESSION['user_info']['id_membre'])
                        );
    $testUtilisateur = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$testUtilisateur){
        setFlashMessage('Vous ne pouvez pas donner un avis sur une salle que vous n\'avez jamais louée.', 'error');
        header('Location: index.php');
        die();
    } else {
            /**
            * Récupérer les infos de la salle
            */
        $query = "SELECT s.titre, s.photo, s.adresse, s.cp, s.ville"
                ." FROM salle s" 
                ." WHERE s.id_salle = " . $pdo->quote($_GET['id']);
        $stmt = $pdo->query($query);
        $salle = $stmt->fetch(PDO::FETCH_ASSOC);
    }
}

$errors = [];
$commentaire = $note = '';

if (!empty($_POST)){
    sanitizePost($_POST);
    extract($_POST);

    if (empty($commentaire)){
        $errors['commentaire'] = 'Vous n\'avez pas entré de commentaire pour cette salle.';
    } elseif (strlen($commentaire) < 10) {
        $errors['commentaire'] = 'Votre commentaire doit faire plus de 10 caractères, merci.';
    } elseif (strlen($commentaire) > 250) {
        $errors['commentaire'] = 'Votre commentaire doit faire moins de 250 caractères, merci.';
    }
    if (empty($note)){
        $errors['note'] = 'Vous n\'avez pas séléctionné de note pour cette salle.';
    }
    
    if(empty($errors)){

        $query = "INSERT INTO avis (id_membre, id_salle, commentaire, note) "
                . "VALUES (:id_membre, :id_salle, :commentaire, :note)"
                ;
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':id_membre', $_SESSION['user_info']['id_membre'], PDO::PARAM_INT);
        $stmt->bindParam(':id_salle', $_GET['id'], PDO::PARAM_INT);
        $stmt->bindParam(':commentaire', $commentaire, PDO::PARAM_STR);
        $stmt->bindParam(':note', $note, PDO::PARAM_INT);
        var_dump($stmt);

        $stmt->execute();
        setFlashMessage('Merci d\'avoir ajouté votre commentaire !');
        header('Location: produit-view.php?id=' .$_GET['produit']);
        die();
    } else {
        setFlashMessage('Erreur(s) présente(s) dans le formulaire', 'error');
    }
}

require 'layout/nav.php';
?>

<div class="container">
    <fieldset>
        <div class="col-md-6">
            <div class="thumbnail">
                <img src="<?= PHOTO_SITE . $salle['photo'] ?>">
                <div class="caption text-center">
                    <h2>Salle <?= $salle['titre'] ?></h2>
                    <p><?= $salle['adresse']?>
                    <?= $salle['cp'] ?>, <?= $salle['ville'] ?></p>
                </div>
            </div>
        </div>
        <legend>Ajouter un avis pour la salle</legend>
        <div class="col-md-6">
        <form method="post">

            <div class="form-group <?= getErrorClass('commentaire', $errors); ?>">
                <label class="control-label" for="commentaire">Avis</label>
                <textarea class="form-control" name="commentaire" placeholder="Entrez un avis ici."></textarea>
                <?= displayErrorMsg('commentaire', $errors) ?>
            </div>

            <div class="form-group <?= getErrorClass('note', $errors); ?>">
                <label class="control-label" for="note">Notez la salle</label>
                <select class="form-control" name="note">
                    <option></option>
                    <option value="1">1 *</option>
                    <option value="2">2 **</option>
                    <option value="3">3 ***</option>
                    <option value="4">4 ****</option>
                    <option value="5">5 *****</option>
                </select>
                <?= displayErrorMsg('note', $errors) ?>
            </div>
            <div class="form-group text-right">
            <button type="submit" class="btn btn-primary">Enregistrer</button>
            </div>
        </form>
        </div>
    </fieldset>
<?php
require '/layout/footer.php'
?>