<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
/**
 * On tente de récupérer le produit portant l'id $_GET['id']
 */
if (isset($_GET)){
	$query = "SELECT p.*, s.titre FROM produit p JOIN salle s ON p.id_salle = s.id_salle WHERE id_produit =" . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$produit = $stmt->fetch();
	/**
	 * Si le résultat est nul, on redirige
	 */
	if (!$produit) {
		redirectMsg('ID produit invalide', 'error', 'produits.php');
	/**
	 * Si quelqu'un tente de rentrer l'id d'une salle ayant une réservation en cours
	 * On redirige + message d'erreur
	 */
	} elseif ($produit['etat'] != 'libre') {
		redirectMsg('Impossible d\'effacer un produit ayant une réservation en cours.', 'error', 'produits.php');
	/**
	 * Sinon, on efface le produit de la base
	 */
	} else {
		if (!empty($_POST)){
			$query = "DELETE FROM produit WHERE id_produit = " . $pdo->quote($_GET['id']);
			$pdo->exec($query);
			
			$message = 'Le produit #' . $produit['id_produit'] . ' (du ' . date("d/m/Y H:i", (strtotime($produit['date_arrivee']))) . ' au ' . date("d/m/Y H:i", (strtotime($produit['date_depart']))) . ') a été supprimé.';
			redirectMsg($message, 'success', 'produits.php');
		}
	}
}

require '../layout/nav.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 thumbnail text-center">
			<p class="text-danger">Vous êtes sur le point d'effacer le produit suivant :</p>

			<p class="text-danger"><strong>Produit #<?= $produit['id_produit']?> - Salle <?= $produit['titre'] ?>. </strong></p>
			<p class="text-danger">Du <?= date("d/m/Y H:i", (strtotime($produit['date_arrivee'])))?> au <?= date("d/m/Y H:i", (strtotime($produit['date_depart']))) ?></p>
			
			<p class="text-danger"><strong>Cette action est définitive.</strong></p>
			<p>Êtes-vous sur de vouloir continuer ?</p>
			
			<a href="produits.php"><button type="button" class="btn btn-success">Retour en arrière</button></a>

			<br><br>
			<p>Ou bien</p>

			<form method="post">
			<button type="submit" class="btn btn-danger" name="effacer-produit">Effacer le produit</button>
			</form>
			<br>
		</div>
	</div>
</div>
<?php 
require '../layout/footer-admin.php';
?>