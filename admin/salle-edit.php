<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';

$errors = [];
$titre = $description = $capacite = $categorie = $pays = $ville = $adresse = $cp = '';

if (!empty($_POST)){

	sanitizePost();
	extract($_POST);

	if (empty($titre)){
		$errors['titre'] = 'Un titre est nécessaire.';
	} elseif (strlen($titre) > 200){
		$errors['titre'] = 'Wow, du calme. Pas plus de 200 caractères dans un nom de salle.';
	}

	if (empty($description)){
		$errors['description'] = 'Une description de la salle est nécessaire.';
	}

	if (!empty($_FILES['photo']['tmp_name'])){
		if ($_FILES['photo']['size'] > 100000){
			$errors['photo'] = 'Le poids de la photo de ne doit pas excéder 100 Ko.';
		}
		$allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
		if (!in_array($_FILES['photo']['type'], $allowedMimeTypes)){
			$errors['photo'] = 'Format de fichier incorrect. Acceptés : .jpg / .png / .gif';
		}
	}

	if (empty($capacite)){
		$errors['capacite'] = 'Une salle doit avoir une capacité.';
	} elseif (!ctype_digit($capacite)){
		$errors['capacite'] = 'La capacité d\'une salle doit être exprimée avec des chiffres.';
	}

	if (empty($adresse)){
		$errors['adresse'] = 'Une adresse de la salle est nécessaire.';
	}

	if (empty($cp)){
		$errors['cp'] = "Vous n'avez pas entré de code postal.";
	} elseif (!ctype_digit($_POST['cp']) || strlen($_POST['cp']) !=5){
		$errors['cp'] = "Le code postal est invalide.";
	}

	if (empty($errors)){
		if (!empty($_FILES['photo']['tmp_name'])){
			$extension = substr($_FILES['photo']['name'], strrpos($_FILES['photo']['name'], '.'));
			$photo = md5(time()) . $extension;
			move_uploaded_file($_FILES['photo']['tmp_name'], PHOTO_SITE . $photo);
			if  (!empty($_POST['anciennePhoto'])) {
				unlink(PHOTO_SITE . $anciennePhoto);
			} 
		} else {
			$photo = $anciennePhoto;
		}

		$query = "UPDATE salle SET titre = :titre, description = :description, photo = :photo, pays = :pays, ville = :ville, adresse = :adresse, cp = :cp, capacite = :capacite, categorie = :categorie WHERE id_salle = :id";
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':titre', $titre, PDO::PARAM_STR);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);
		$stmt->bindParam(':photo', $photo, PDO::PARAM_STR);
		$stmt->bindParam(':pays', $pays, PDO::PARAM_STR);
		$stmt->bindParam(':ville', $ville, PDO::PARAM_STR);
		$stmt->bindParam(':adresse', $adresse, PDO::PARAM_STR);
		$stmt->bindParam(':cp', $cp, PDO::PARAM_INT);
		$stmt->bindParam(':capacite', $capacite, PDO::PARAM_INT);
		$stmt->bindParam(':categorie', $categorie, PDO::PARAM_STR);
		$stmt->bindParam('id', $_GET['id'], PDO::PARAM_STR);
		$stmt->execute();
		setFlashMessage('Salle modifiée avec succès.');
	} else {
		setFlashMessage('Erreur(s) présente(s) dans le formulaire.', 'error');		
	}	
}

if (!empty($_GET)){
	$query = "SELECT * FROM salle WHERE id_salle = " . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$infoSalle = $stmt->fetch(PDO::FETCH_ASSOC);
	if (!$infoSalle){
		header('Location: index.php');
	}
} else {
	header('Location: index.php');
}

require '../layout/nav.php';
?>

<div class="container">
<?= displayFlashMessage() ?>
<fieldset>
	<legend>Editer les information de la salle</legend>
	<form method="post" enctype="multipart/form-data">

		<div class="form-group <?= getErrorClass('titre', $errors) ?>">
		 	<label class="control-label" for="titre">Titre</label>
		 	<div>
		 		<input type="text" class="form-control" name="titre" placeholder="Titre de la salle" value="<?= $infoSalle['titre']?>">
		 		<?= displayErrorMsg('titre', $errors) ?> 
		 	</div>
		 </div>	

		 <div class="form-group <?= getErrorClass('description', $errors) ?>">
		 	<label class="control-label" for="description">Description</label>
		 	<div>
		 		<textarea class="form-control" name="description" placeholder="Description de la salle"><?= $infoSalle['description']?></textarea>
		 		<?= displayErrorMsg('description', $errors) ?> 
		 	</div>
		 </div>

	 	<div class="form-group <?= getErrorClass('photo', $errors) ?>">
	 		<label class="control-label" for="photo">Photo</label>
	 		<div>
	 			<input type="file" class="form-control" name="photo">
	 			<?= displayErrorMsg('photo', $errors) ?>
	 			<input type="hidden" name="anciennePhoto" value="<?= $infoSalle['photo'] ?>" > 
	 		</div>
		</div>
		<div class="thumbnail">
			<img class="thumbnail-edit img-thumbnail" src="<?= PHOTO_SITE . $infoSalle['photo'] ?>">
		</div>

		 <div class="form-group <?= getErrorClass('capacite', $errors) ?>">
		 	<label class="control-label" for="capacite">Capacité</label>
			<div>
				<input type="text" class="form-control" name="capacite" placeholder="Capacité de la salle" value="<?= $infoSalle['capacite']?>">
				<?= displayErrorMsg('capacite', $errors) ?> 
		 	</div>
		 </div>	

	 	<div class="form-group <?= getErrorClass('categorie', $errors) ?>">
		 	<label class="control-label" for="categorie">Catégorie</label>
			<div>
				<select class="form-control" name="categorie">
					<option value="reunion" <?= $infoSalle['categorie'] == 'reunion' ? 'selected' : '' ?>>Réunion</option>
					<option value="bureau" <?= $infoSalle['categorie'] == 'bureau' ? 'selected' : '' ?>>Bureau</option>
					<option value="formation" <?= $infoSalle['categorie'] == 'formation' ? 'selected' : '' ?>>Formation</option>
				</select>
				<?= displayErrorMsg('categorie', $errors) ?>
	 		</div>
	 	</div>	

	 	<div class="form-group <?= getErrorClass('pays', $errors) ?>">
	 		<label class="control-label" for="pays">Pays</label>
	 		<div>
				<select class="form-control" name="pays">
					<option value="France">France</option>
				</select>
				<?= displayErrorMsg('pays', $errors) ?>
	 		</div>
	 	</div>	

	 	<div class="form-group <?= getErrorClass('ville', $errors) ?>">
	 		<label class="control-label" for="ville">Ville</label>
	 		<div>
				<select class="form-control" name="ville">
					<option value="Paris" <?= $infoSalle['ville'] == 'Paris' ? 'selected' : '' ?>>Paris</option>
					<option value="Lyon" <?= $infoSalle['ville'] == 'Lyon' ? 'selected' : '' ?>>Lyon</option>
					<option value="Marseille" <?= $infoSalle['ville'] == 'Marseille' ? 'selected' : '' ?>>Marseille</option>
				</select>
				<?= displayErrorMsg('ville', $errors) ?>
	 		</div>
	 	</div>

	 	<div class="form-group <?= getErrorClass('adresse', $errors) ?>">
	 		<label class="control-label" for="adresse">Adresse</label>
	 		<div>
	 			<textarea class="form-control" name="adresse" placeholder="Adresse de la salle"><?= $infoSalle['adresse']?></textarea>
	 			<?= displayErrorMsg('adresse', $errors) ?> 		 		
	 		</div>
	 	</div>

		<div class="form-group <?= getErrorClass('cp', $errors) ?>">
		 	<label class="control-label" for="cp">Code postal</label>
		 	<div>
		 		<input type="text" class="form-control" name="cp" placeholder="Code postal de la salle" value="<?= $infoSalle['cp']?>">
		 		<?= displayErrorMsg('cp', $errors) ?> 
	 		</div>
	 	</div>

	 	<button type="submit" class="btn btn-default">Enregistrer</button>	
	</form>
</fieldset>
</div>
<?php 
require '../layout/footer-admin.php';
?>