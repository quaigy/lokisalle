<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
/**
 * On tente de récupérer l'avis' portant l'id $_GET['id']
 */
if (isset($_GET)){
	$query = "SELECT a.*, m.nom, m.prenom, s.titre "
			."FROM avis a "
			."JOIN membre m USING (id_membre) "
			."JOIN salle s USING (id_salle) "
			."WHERE a.id_avis =" . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$avis = $stmt->fetch();
	/**
	 * Si le résultat est nul, on redirige
	 */
	if (!$avis) {
		redirectMsg('ID avis invalide', 'error', 'avis.php');

	/**
	 * Sinon, on efface l'avis  de la base
	 */
	} else {
		if (!empty($_POST) && isset($_POST['effacer-avis'])){
			$query = "DELETE FROM avis WHERE id_avis = " . $pdo->quote($_GET['id']);
			$pdo->exec($query);
			
			$message = 'L\'avis #' . $avis['id_avis'] . ' (par ' . $avis['prenom'] . '' . $avis['nom'] . ', le ' . formatDateJMA($commande['date_enregistrement']) . ') a été supprimée.';
			redirectMsg($message, 'success', 'avis.php');

		}
	}
}

require '../layout/nav.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 thumbnail text-center">
			<p class="text-danger">Vous êtes sur le point d'effacer l'avis suivant :</p>

			<p class="text-danger"><strong>Avis #<?= $avis['id_avis']?> - Salle #<?= $avis['id_salle'] ?> - <?= $avis['titre'] ?>. </strong></p>
			<div class="well">
			
			<p class="text-danger"><?= $avis['commentaire'] ?></p>
			<?= afficherEtoiles($avis['note']) ?>
			</div>
			<p class="text-danger"><strong>Cette action est définitive.</strong></p>
			<p>Êtes-vous sur de vouloir continuer ?</p>
			
			<a href="commandes.php"><button type="button" class="btn btn-success">Retour en arrière</button></a>

			<br><br>
			<p>Ou bien</p>

			<form method="post">
			<button type="submit" class="btn btn-danger" name="effacer-avis">Effacer la commande</button>
			</form>
			<br>
		</div>
	</div>
</div>

<?php 
require '../layout/footer-admin.php';
?>