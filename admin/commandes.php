<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
/**
 * Gérer l'affichage des détails d'une commande
 */
$testGet = '';
if (isset($_GET['id'])){
	$query = "SELECT id_commande FROM commande WHERE id_commande =" . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$testGet = $stmt->fetch();

	if(!$testGet){
		setFLashMessage('Commande inexistante.', 'error');
	} else {
		$query = "SELECT c.*, p.date_arrivee, p.date_depart, p.prix, s.titre, s.photo, s.pays, s.ville, "
				. " s.adresse, s.cp, s.capacite, s.categorie, m.nom, m.prenom, m.civilite, m.email"
				. " FROM commande c"
				. " JOIN produit p"
				. " ON c.id_produit = p.id_produit"
				. " JOIN salle s"
				. " ON p.id_salle = s.id_salle"
				. " JOIN membre m"
				. " ON c.id_membre = m.id_membre"
				. " WHERE c.id_commande = " . $pdo->quote($_GET['id'])
				;
		$stmt = $pdo->query($query);
		$detail = $stmt->fetch(PDO::FETCH_ASSOC);
	}
}

/**
* Peupler le tableau des commandes
*/
$query = "SELECT c.*, p.date_arrivee, p.date_depart, p.prix, s.titre, s.photo, m.civilite, m.email "
		. "FROM commande c "
		. "JOIN produit p "
		. "ON c.id_produit = p.id_produit "
		. "JOIN salle s "
		. "ON p.id_salle = s.id_salle "
		. "JOIN membre m "
		. "ON c.id_membre = m.id_membre "
		;
$results = ['id_commande' => 'commande', 
			'id_produit' => 'commande', 
			'id_membre' => 'commande',  
			'prix' => 'produit', 
			'date_enregistrement' => 'commande'];
$query .= getOrderBy($results);

/**
* Pagination - intercallé avant l'execution de la requête afin de 
* gerer l'ajout de LIMIT dans $query
 */
if (isset($_GET['parPage'])){ 
	$parPage = $_GET['parPage'];
} else {
	$parPage = 5; // nombres de résultats que l'on souhaite afficher par page
} 
list($nbPages, $pageActuelle, $debutPagination) = pagination($pdo, 'commande', $parPage);

if (isset($_GET['page'])){
		$query .=  " LIMIT " . $debutPagination . ", ". $parPage;
} else {
		$query .= " LIMIT 0, " . $parPage;
}
/**
 * Fin De la pagination, on execute la requête ensuite
 */

$stmt = $pdo->query($query);
$commandes = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>

<div class="container">
	<?= displayFlashMessage() ?>
	<fieldset id="table">
		<legend>Toutes les commandes</legend>
		<div class="row">
		<!-- classer l'affichage des résultats -->
		<div class="col-md-8">
		<form class="form-inline pb-10" method="get">
			<div class="form-group">
				<label class="control-label" for="classer">Classer par :</label>
				<select class="form-control" name="classer">
					<!-- on foreach $results qui contient les noms des colonnes du tableau -->
					<?php
					foreach ($results as $id => $table) :

					?>
					<option value="<?= $id ?>" <?= isset($_GET['classer']) && $_GET['classer'] == $id ? 'selected' : ''?> ><?= $id ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" name="ordre">
					<option value="ASC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'ASC' ? 'selected' : '' ?>>ascendant</option>
					<option value="DESC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'DESC' ? 'selected' : '' ?>>descendant</option>
				</select>
			</div>
			<div class="form-group">
				<label> Résultats par page :</label>
				<select class="form-control" name="parPage">
					<?php
					for($i=5; $i<20; $i+=5) :
					?>
						<option value="<?= $i ?>" <?= isset($_GET['parPage']) && $_GET['parPage'] == $i ? 'selected' : '' ?>><?= $i ?></option>
					<?php
					endfor;
					?>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Classer</button>
		</form>
		</div>
		<!-- pagination -->
		<div class="col-md-4 pull-right">
			<nav id="pagination" aria-label="Pagination">

				<ul class="pagination no-margin pb-10 pull-right">
					<?php
					for ($i = 1; $i <= $nbPages; $i++):
						$recupereGet = '';
						$recupereGet .= isset($_GET['classer']) ? '&classer=' . $_GET['classer'] : '';
						$recupereGet .= isset($_GET['ordre']) ? '&ordre='. $_GET['ordre'] : '';
						$recupereGet .= isset($_GET['parPage']) ? '&parPage='. $_GET['parPage'] : '';
					?>
						<?= ($i == $pageActuelle) ? '<li class="active"><a href="">'.$i.'</a></li>' :
						'<li><a href="'.RACINE_SITE.'admin/commandes.php?page='.$i .$recupereGet.'">'.$i. '</a></li>' ?>	
					<?php
					endfor;
					?>
				</ul>
			</nav>
		</div>
		<!-- fin pagination -->
		<!-- affichage des commandes-->
		<table class="table table-stripped table-bordered table-condensed">
			<tr>
				<th class="text-center inverse">Id commande</th>
				<th class="text-center inverse">Produit</th>
				<th class="text-center inverse">Acheteur</th>
				<th class="text-center inverse">Prix</th>
				<th class="text-center inverse">Date enregistrement</th>
				<th class="text-center inverse">Actions</th>
			</tr>
		<?php
		foreach ($commandes as $commande) :
		?>
			<tr class="text-center">
				<td><?= $commande['id_commande'] ?></td>
				<td>
					<p>Produit # <strong><?= $commande['id_produit']?></strong> - Salle <?= $commande['titre'] ?></p>
					<p>Du <?= formatDateJMA($commande['date_arrivee']) ?><br>au <?= formatDateJMA($commande['date_depart']) ?></p>
				</td>
				<td>
					<p>Membre # <strong><?= $commande['id_membre'] ?></strong></p>
					<p><?= $commande['email'] ?></p>
				</td>
				<td><?= $commande['prix'] ?> €</td>
				<td><?= $commande['date_enregistrement'] ?></td>
				<!-- Je n'ai pas mis d'option pour modifier les commandes, ca me semblait inutile
					 dans ce contexte, même si c'était dans le cahier des charges -->
				<td><a href="<?= RACINE_SITE . 'admin/commandes.php?id='. $commande['id_commande'] ?>#details" class="btn btn-primary btn-sm" title="Voir les détails"><span class="glyphicon glyphicon-search" title="Voir la fiche produit"></span></a>&nbsp;
				<a href="<?= RACINE_SITE . 'admin/commande-delete.php?id='. $commande['id_commande'] ?>" class="btn btn-danger btn-sm" title="Editer le produit"><span class="glyphicon glyphicon-trash" title="Supprimer"></span></a></td>
			</tr>
		<?php
		endforeach;
		?>
		</table>


		<!-- Affichage des détails d'une commande -->
		<?php
		if (!empty($testGet)) :
		?>
		<a name="details"></a>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Détails de la commande # <?= $detail['id_commande'] ?><span class="pull-right"><span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;passée le <?= formatDateJMA($detail['date_enregistrement']) ?></span></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2">
						<img class="thumbnail thumbnail-commande" src="<?= PHOTO_SITE . $detail['photo'] ?>">
					</div>
					<div class="col-md-4 col-md-offset-1">
						<p><strong>Client :</strong></p>
						<p><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?= ($detail['civilite'] == 'm') ? 'Monsieur' : 'Madame' ?><?=  ' '. $detail['nom'] . ' ' . $detail['prenom']?></p>
						<p><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<?= $detail['email'] ?></p>
						<p><strong>Dates :</strong></p>
						<p><span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Du <?= $detail['date_arrivee'] ?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;au <?= $detail['date_depart'] ?></p>
					</div>
					<div class="col-md-4 col-md-offset-1">
						
						<p><strong>Infos salles :</strong></p>
						<p>Salle <?= $detail['titre'] ?> - <?= $detail['categorie'] ?></p>
						<p><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<?= $detail['capacite'] ?> personnes.</p>
						<p><?= $detail['adresse']?></p>
						<p><?= $detail['cp'] ?>, <?= $detail['ville'] ?></p>
						
					</div>
					<div class="col-md-9 col-md-offset-3">
						<p><span class="pull-right"><strong>Tarif : <?= $detail['prix'] ?></strong>&nbsp;
							<span class="glyphicon glyphicon-euro"></span></span>
						</p>
					</div>
				</div>
			</div>
		</div>
		<?php
		endif;
		?>
	</fieldset>
</div>
<?php 
require '../layout/footer-admin.php';
?>