<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
if (isset($_GET)){
	$query = "SELECT id_salle, titre, photo FROM salle WHERE id_salle =" . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$salle = $stmt->fetch();

	if (!$salle) {
		header('Location: salles.php');
	} else {
		if (!empty($_POST)){
			$query = "DELETE FROM salle WHERE id_salle = " . $pdo->quote($_GET['id']);
			$pdo->exec($query);
			
			unlink(PHOTO_SITE . $salle['photo']);
			setFlashMessage('La salle #' . $salle['id_salle'] . ' - <strong>' . $salle['titre'] .'</strong> a été supprimée.');
			header('Location: salles.php');
		}
	}
}

require '../layout/nav.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 thumbnail text-center">
			<p class="text-danger">Vous êtes sur le point d'effacer la salle suivante :</p>

			<p class="text-danger"><strong><?= $salle['titre']?></strong></p>
			<img class="thumbnail-delete img-thumbnail" src="<?= PHOTO_SITE . $salle['photo'] ?>">
			
			<p class="text-danger"><strong>Cette action est définitive.</strong></p>
			<p>Êtes-vous sur de vouloir continuer ?</p>
			
			<a href="salles.php"><button type="button" class="btn btn-success">Retour en arrière</button></a>

			<br><br>
			<p>Ou bien</p>

			<form method="post">
			<button type="submit" class="btn btn-danger" name="effacer-salle">Effacer la salle</button>
			</form>
			<br>
		</div>
	</div>
</div>
<?php 
require '../layout/footer-admin.php';
?>