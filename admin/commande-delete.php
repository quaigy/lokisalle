<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
/**
 * On tente de récupérer la commande portant l'id $_GET['id']
 */
if (isset($_GET)){
	$query = "SELECT c.*, p.date_arrivee, p.date_depart FROM commande c JOIN produit p ON c.id_produit = p.id_produit WHERE c.id_commande =" . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$commande = $stmt->fetch();
	/**
	 * Si le résultat est nul, on redirige
	 */
	if (!$commande) {
		redirectMsg('ID commande invalide', 'error', 'commandes.php');
	/**
	 * Sinon, on efface la commande de la base
	 * et on met à jour l'état du produit.
	 */
	} else {
		if (!empty($_POST) && isset($_POST['effacer-commande'])){
			$query = "DELETE FROM commande WHERE id_commande = " . $pdo->quote($_GET['id']);
			$pdo->exec($query);

			$query = "UPDATE produit SET etat = 'libre' WHERE id_produit = " . $pdo->quote($_GET['id']);
			$stmt = $pdo->query($query);
			$stmt->execute();
			
			$message = 'La commande #' . $commande['id_commande'] . ' du ' . date("d/m/Y H:i", (strtotime($commande['date_enregistrement']))) . ' au ' . date("d/m/Y H:i", (strtotime($commande['date_depart']))) . ' a été supprimée.';
			redirectMsg($message, 'success', 'commandes.php');

		}
	}
}

require '../layout/nav.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 thumbnail text-center">
			<p class="text-danger">Vous êtes sur le point d'effacer la commande suivante :</p>

			<p class="text-danger"><strong>Commande #<?= $commande['id_commande']?> - Produit #<?= $commande['id_produit'] ?>. </strong></p>
			<p class="text-danger">Du <?= date("d/m/Y H:i", (strtotime($commande['date_arrivee'])))?> au <?= date("d/m/Y H:i", (strtotime($commande['date_depart']))) ?></p>
			
			<p class="text-danger"><strong>Cette action est définitive.</strong></p>
			<p>Êtes-vous sur de vouloir continuer ?</p>
			
			<a href="commandes.php"><button type="button" class="btn btn-success">Retour en arrière</button></a>

			<br><br>
			<p>Ou bien</p>

			<form method="post">
			<button type="submit" class="btn btn-danger" name="effacer-commande">Effacer la commande</button>
			</form>
			<br>
		</div>
	</div>
</div>
<?php 
require '../layout/footer-admin.php';
?>