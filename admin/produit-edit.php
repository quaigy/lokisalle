<?php
require '../include/init.php';
adminSecurity();
/**
* Charger les CSS & JS pour activer le TimePicker dans
* le  header.php et footer.php
*/
$loadTimePicker = '';
require '../layout/header.php';

/**
* Gestion des erreurs
*/
$errors = [];
$date_depart = $date_arrivee = $id_salle = $prix = '';

if (!empty($_POST)){
	sanitizePost();
	extract($_POST);

	if (empty($date_arrivee)){
		$errors['date_arrivee'] = 'Vous n\'avez pas indiqué une date d\'arrivée.';
	} elseif (strtotime($date_arrivee) == false){
		$errors['date_arrivee'] = 'Format de date invalide.';
	} elseif (strtotime($date_arrivee) > strtotime($date_depart)){
		$errors['date_arrivee'] = 'La date d\'arrivée ne peut pas être postérieure à la date de départ.';
	} elseif (strtotime($date_arrivee) < time()){
		$errors['date_arrivee'] = 'La date d\'arrivée ne peut pas être antérieure à la date du jour.';
	}

	if (empty($date_depart)){
		$errors['date_depart'] = 'Vous n\'avez pas indiqué une date de départ.';
	} elseif (strtotime($date_depart) == false){
		$errors['date_depart'] = 'Format de date invalide.';
	}

	if (strtotime($date_depart) == strtotime($date_arrivee)){
		$errors['date_depart'] = 'La date de départ ne peut pas être la même que la date d\'arrivée.';
	} elseif (strtotime($date_depart) < strtotime($date_arrivee)) {
		$errors['date_depart'] = 'La date de départ ne peut pas être antérieure à la date d\'arrivée.';
	} elseif (strtotime($date_depart) < time()){
		$errors['date_depart'] = 'La date de départ ne peut pas être antérieure à la date du jour.';
	}

	if (empty($id_salle)){
		$errors['id_salle'] = 'Vous n\'avez pas selectionné une salle.';
	}

	if (empty($prix)){
		$errors['prix'] = 'Vous n\'avez pas entré de prix.';
	} elseif (!ctype_digit($prix)){
		$errors['prix'] = 'Le prix ne doit contenir que des chiffres.';
	}

	if (empty($errors)){
		/**
		 * On vérifie l'absence de conflits de dates avec les autres produits
		 * existants. 
		 * On exclue le produit en cours de modification de la recherche.
		 */
		$query = "SELECT * FROM produit" 
					. " WHERE id_salle = :id_salle"
					. " AND id_produit != :id_produit"
					. " AND ((date_arrivee BETWEEN :date_arrivee AND :date_depart)"
					. " OR (date_depart BETWEEN :date_arrivee AND :date_depart))"
					;
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':id_produit', $_GET['id'], PDO::PARAM_INT);
		$stmt->bindParam(':id_salle', $id_salle, PDO::PARAM_INT);
		$stmt->bindParam(':date_arrivee', $date_arrivee, PDO::PARAM_STR);
		$stmt->bindParam(':date_depart', $date_depart, PDO::PARAM_STR);			
		$stmt->execute();
		$doublons = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (!empty($doublons)){
			setFlashMessage('Il existe déjà un produit aux dates séléctionnées.', 'error');
		} else {
			/**
			 * Si pas de doublons, ajout dans la base
			 */
			$etat= 'libre';
			$query = "UPDATE produit SET date_arrivee = :date_arrivee, date_depart = :date_depart, prix = :prix, etat = :etat WHERE id_produit = :id_produit";
			$stmt = $pdo->prepare($query);
			$stmt->bindParam(':id_produit', $_GET['id'], PDO::PARAM_INT);
			$stmt->bindParam(':date_arrivee', $date_arrivee, PDO::PARAM_STR);
			$stmt->bindParam(':date_depart', $date_depart, PDO::PARAM_STR);		
			$stmt->bindParam(':prix', $prix, PDO::PARAM_INT);
			$stmt->bindParam(':etat', $etat, PDO::PARAM_STR);
			$stmt->execute();
			setFlashMessage('Salle modifiée avec succès.');
		}
	} else {
		setFlashMessage('Erreur(s) présente(s) dans le formulaire.', 'error');		
	}	
}
/**
 *  Si on arrive par un lien correct (ie. ?id='xxx')
 *  On récupère les infos du produit correspondant à l'id
 *  Si l'id ne correspond pas à un produit dans la base ou 
 *  qu'on tente d'accèder à la page sans ?id='xxx',
 *  redirection vers l'index
 */

if (!empty($_GET)){
	$query = "SELECT * FROM produit WHERE id_produit = " . $pdo->quote($_GET['id']);
	$stmt = $pdo->query($query);
	$infoProduit = $stmt->fetch(PDO::FETCH_ASSOC);
	if (!$infoProduit){
		header('Location: index.php');
	}
} else {
	header('Location: index.php');
}

/**
 * Requête des salles pour peupler le <select>
 */
$stmt = $pdo->query("SELECT * FROM salle");
$salles = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>

<div class="container">
<?= displayFlashMessage() ?>
	<fieldset>
		<legend>Modifier un produit</legend>
		<form method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('date_arrivee', $errors) ?>">
						<label for="date_arrivee" class="control-label">Date d'arrivée</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="text" id="date_arrivee" class="form-control" name="date_arrivee" placeholder="YYYY-mm-dd hh:mm:ss" value="<?= $infoProduit['date_arrivee'] ?>"/>
							
							</div>
							<div><?= displayErrorMsg('date_arrivee', $errors) ?></div>
							
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('id_salle', $errors) ?>">
						<label for="id_salle" class="control-label">Salle</label>
							<select name="id_salle" class="form-control">
							<option value="">Selectionnez une salle : </option>
							<?php
							foreach ($salles as $salle) :
								$concat = $salle['id_salle'] . ' - Salle ' . $salle['titre']  . ' - ' . $salle['adresse'] . ', ' . $salle['cp'] . ', ' . $salle['ville'] . ' - ' . $salle['capacite'] . ' pers';
							?>
								<option value="<?= $salle['id_salle'] ?>" <?= ($infoProduit['id_salle'] == $salle['id_salle']) ? 'selected' : '' ?>><?= $concat ?></option>
							<?php
							endforeach;
							?>
							</select>
							<div><?= displayErrorMsg('id_salle', $errors) ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('date_depart', $errors) ?>">
						<label for="date_depart" class="control-label">Date de départ</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="text" id="date_depart" class="form-control" name="date_depart" placeholder="YYYY-mm-dd hh:mm:ss" value="<?= $infoProduit['date_depart'] ?>"/>
							</div>
							<div><?= displayErrorMsg('date_depart', $errors) ?></div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('prix', $errors) ?>">
						<label for="prix" class="control-label">Tarif</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-eur"></span></span>
							<input type="text" class="form-control" name="prix" placeholder="Prix en euros" value="<?= $infoProduit['prix'] ?>"/>
							</div>
							<div><?= displayErrorMsg('prix', $errors) ?></div>
					</div>
				</div>
				<div class="col-md-12 text-right">
				<button class="btn btn-primary">Mettre à jour</button>
				</div>

			</div>
		</form>
	</fieldset>
</div>
<?php 
require '../layout/footer-admin.php';
?>