<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';

/**
* Peupler le tableau des avis
*/
$query = "SELECT a.*, m.email, s.titre "
		. "FROM avis a "
		. "JOIN membre m "
		. "ON a.id_membre = m.id_membre "
		. "JOIN salle s "
		. "ON a.id_salle = s.id_salle "
		;
$results = ['id_avis' => 'avis', 
			'id_membre' => 'avis', 
			'id_salle' => 'avis',  
			'note' => 'avis', 
			'date_enregistrement' => 'avis'];
$query .= getOrderBy($results);
/**
* Pagination - intercallé avant l'execution de la requête afin de 
* gerer l'ajout de LIMIT dans $query
 */
if (isset($_GET['parPage'])){ 
	$parPage = $_GET['parPage'];
} else {
	$parPage = 5; // nombres de résultats que l'on souhaite afficher par page
} 
list($nbPages, $pageActuelle, $debutPagination) = pagination($pdo, 'commande', $parPage);

if (isset($_GET['page'])){
		$query .=  " LIMIT " . $debutPagination . ", ". $parPage;
} else {
		$query .= " LIMIT 0, " . $parPage;
}
/**
 * Fin De la pagination, on execute la requête ensuite
 */

$stmt = $pdo->query($query);
$avisMembres = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>

<div class="container">
	<?= displayFlashMessage() ?>
	<fieldset id="table">
		<legend>Toutes les avis</legend>
		<div class="row">
		<!-- classer l'affichage des résultats -->
		<div class="col-md-8">
		<form class="form-inline pb-10" method="get">
			<div class="form-group">
				<label class="control-label" for="classer">Classer par :</label>
				<select class="form-control" name="classer">
					<!-- on foreach $results qui contient les noms des colonnes du tableau -->
					<?php
					foreach ($results as $id => $table) :

					?>
					<option value="<?= $id ?>" <?= isset($_GET['classer']) && $_GET['classer'] == $id ? 'selected' : ''?> ><?= $id ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" name="ordre">
					<option value="ASC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'ASC' ? 'selected' : '' ?>>ascendant</option>
					<option value="DESC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'DESC' ? 'selected' : '' ?>>Descendant</option>
				</select>
			</div>
			<div class="form-group">
				<label> Résultats par page :</label>
				<select class="form-control" name="parPage">
					<?php
					for($i=5; $i<20; $i+=5) :
					?>
						<option value="<?= $i ?>" <?= isset($_GET['parPage']) && $_GET['parPage'] == $i ? 'selected' : '' ?>><?= $i ?></option>
					<?php
					endfor;
					?>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Classer</button>
		</form>
		</div>
		<!-- pagination -->
		<div class="col-md-4 pull-right">
			<nav id="pagination" aria-label="Pagination">

				<ul class="pagination no-margin pb-10 pull-right">
					<?php
					for ($i = 1; $i <= $nbPages; $i++):
						$recupereGet = '';
						$recupereGet .= isset($_GET['classer']) ? '&classer=' . $_GET['classer'] : '';
						$recupereGet .= isset($_GET['ordre']) ? '&ordre='. $_GET['ordre'] : '';
						$recupereGet .= isset($_GET['parPage']) ? '&parPage='. $_GET['parPage'] : '';
					?>
						<?= ($i == $pageActuelle) ? '<li class="active"><a href="">'.$i.'</a></li>' :
						'<li><a href="'.RACINE_SITE.'admin/avis.php?page='.$i .$recupereGet.'">'.$i. '</a></li>' ?>	
					<?php
					endfor;
					?>
				</ul>
			</nav>
		</div>
		<!-- fin pagination -->
		<!-- affichage des avis -->
		<table class="table table-stripped table-bordered table-condensed">
			<tr>
				<th class="text-center inverse">Id avis</th>
				<th class="text-center inverse">Id membre</th>
				<th class="text-center inverse">Id salle</th>
				<th class="text-center inverse">Commentaire</th>
				<th class="text-center inverse">Note</th>
				<th class="text-center inverse">Date enregistrement</th>
				<th class="text-center inverse">Actions</th>
			</tr>
		<?php
		foreach ($avisMembres as $avis) :
		?>
			<tr class="text-center">
				<td><?= $avis['id_avis'] ?></td>
				<td class="text-nowrap"><strong><?= $avis['id_membre']?></strong> - <?= $avis['email'] ?></td>
				<td><?= $avis['id_salle'] ?></td>
				<td style="width : 25%" class="text-justify"><?= $avis['commentaire'] ?></td>
				<td class="text-nowrap"><?= afficherEtoiles($avis['note']) ?></td>
				<td class="text-nowrap"><?= formatDateJMA($avis['date_enregistrement']); ?></td>
				<!-- Pas d'option pour voir un avis particulier car ils
					sont déjà affichés entièrement dans le tableau -->
				<td class="text-nowrap"><a href="<?= RACINE_SITE . 'admin/avis.php?id='. $avis['id_avis'] ?>" class="btn btn-primary btn-sm" title="Editer l'avis"><span class="glyphicon glyphicon-edit" title="Editer l'avis"></span></a>&nbsp;
				<a href="<?= RACINE_SITE . 'admin/avis-delete.php?id='. $avis['id_avis'] ?>" class="btn btn-danger btn-sm" title="Effacer l'avis'"><span class="glyphicon glyphicon-trash" title="Supprimer"></span></a></td>
			</tr>
		<?php
		endforeach;
		?>
		</table>
		<!-- fin affichage avis-->
	</fieldset>
</div>

<?php 
require '../layout/footer-admin.php';
?>