<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
$testId = '';
/**
 * Si on a une id membre, on affiche son contenu sous le tableau des résultats
 */
if (isset($_GET['id'])){
	sanitizeArray($_GET);
	$stmt = $pdo->query("SELECT * FROM membre WHERE id_membre = ". $pdo->quote($_GET['id']));
	$testId = $stmt->fetch(PDO::FETCH_ASSOC);
	if (!$testId){
		setFlashMessage('Membre inconnu.', 'error');
	} else {
		$errors = [];
		$id_membre = $pseudo = $nom = $prenom = $email = $civilite = $statut = '';
		extract($testId);

		if (!empty($_POST)){
			sanitizePost();
			extract($_POST);

			if (empty($pseudo)){
				$errors['pseudo'] = 'Un pseudonyme est obligatoire.';
			} elseif (strlen($pseudo) < 6){
				$errors['pseudo'] = 'Le pseudo doit contenir plus de 6 caractères.';
			} elseif (strlen($pseudo) > 20){
				$errors['pseudo'] = 'Le pseudo doit contenir moins de 20 caractères.';
			} elseif ( !preg_match('/^[a-zA-Z0-9_-]{6,20}$/',$pseudo)) {
				$errors['pseudo'] = 'Caractères acceptés : chiffres, lettres, - et _ .';
			} else {
				$query = "SELECT COUNT(*) FROM membre WHERE pseudo = :pseudo AND id_membre != :id_membre";
				$stmt = $pdo->prepare($query);
				$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
				$stmt->bindParam(':id_membre', $id_membre, PDO::PARAM_INT);
				$stmt->execute();
				if ($stmt->fetchColumn() == 1){
					$errors['pseudo'] = 'Ce pseudo existe déjà. Merci d\'en choisir un autre.';
				}
			}
			if (empty($prenom)){
				$errors['prenom'] = 'Un prénom est obligatoire.';
			} elseif (strlen($prenom) > 20){
				$errors['prenom'] = 'Le prénom doit faire moins de 20 caractères.';
			} 

			if (empty($email)){
				$errors['email'] = 'Un email est obligatoire';
			} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$errors['email'] = 'Format d\'email invalide.';
			} else {
				$query = "SELECT COUNT(*) FROM membre WHERE email = :email AND id_membre != :id_membre";
				$stmt = $pdo->prepare($query);
				$stmt->bindParam(':email', $email, PDO::PARAM_STR);
				$stmt->bindParam(':id_membre', $id_membre, PDO::PARAM_INT);
				$stmt->execute();
				if ($stmt->fetchColumn() == 1){
					$errors['email'] = 'Cet email existe déjà. Merci d\' en utiliser un autre.';
				}
			}

			if (empty($errors)){
				$query = 'UPDATE membre SET pseudo = :pseudo, nom = :nom, prenom = :prenom, email = :email, civilite = :civilite, statut = :statut WHERE id_membre = :id_membre';
				$stmt = $pdo->prepare($query);
				$stmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
				$stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
				$stmt->bindParam(':prenom', $prenom, PDO::PARAM_STR);
				$stmt->bindParam(':email', $email, PDO::PARAM_STR);
				$stmt->bindParam(':civilite', $civilite, PDO::PARAM_STR);
				$stmt->bindParam(':statut', $statut, PDO::PARAM_INT);
				$stmt->bindParam(':id_membre', $id_membre, PDO::PARAM_STR);
				$stmt->execute();

				setFlashMessage('Les informations du membre #' .$id_membre. ' ont bien étés modifiées.');
			} else {
				setFlashMessage('Erreur(s) présente(s) dans le formulaire', 'error');
			}
		}
	}
}

$query = "SELECT * FROM membre m ";
$results = ['id_membre' => 'membre', 
			'pseudo' => 'membre', 
			'nom' => 'membre',  
			'prenom' => 'membre', 
			'email' => 'membre', 
			'civilite' => 'membre', 
			'statut' => 'membre', 
			'date_enregistrement' => 'membre'];
$query .= getOrderBy($results);

/**
* Pagination - intercallé avant l'execution de la requête afin de 
* gerer l'ajout de LIMIT dans $query
 */
$parPage = 5; // nombres de résultats que l'on souhaite afficher par page
list($nbPages, $pageActuelle, $debutPagination) = pagination($pdo, 'membre', $parPage);

if (isset($_GET['page'])){
		$query .=  " LIMIT " . $debutPagination . ", ". $parPage;
} else {
		$query .= " LIMIT 0, " . $parPage;
}
/**
 * Fin De la pagination, on execute la requête ensuite
 */
$stmt = $pdo->query($query);
$membres = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>
<div class="container">
	<?= displayFlashMessage() ?>
	<fieldset id="table">
		<legend>Touts les membres</legend>
		<div class="row">
		<!-- classer l'affichage des résultats -->
		<div class="col-md-6">
		<form class="form-inline pb-10" method="get">
			<div class="form-group">
				<label class="control-label" for="classer">Classer par :</label>
				<select class="form-control" name="classer">
					<!-- on foreach $results qui contient les noms des colonnes du tableau -->
					<?php
					foreach ($results as $id => $table) :

					?>
					<option value="<?= $id ?>" <?= isset($_GET['classer']) && $_GET['classer'] == $id ? 'selected' : ''?> ><?= $id ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" name="ordre">
					<option value="ASC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'ASC' ? 'selected' : '' ?>>ordre ascendant</option>
					<option value="DESC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'DESC' ? 'selected' : '' ?>>ordre descendant</option>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Classer</button>
		</form>
		</div>
		<!-- pagination -->
		<div class="col-md-6 pull-right">
			<nav id="pagination" aria-label="Pagination">

				<ul class="pagination no-margin pb-10 pull-right">
					<?php
					for ($i = 1; $i <= $nbPages; $i++):
						$recupereGet = '';
						$recupereGet .= isset($_GET['classer']) ? '&classer=' . $_GET['classer'] : '';
						$recupereGet .= isset($_GET['ordre']) ? '&ordre='. $_GET['ordre'] : '';
					?>
						<?= ($i == $pageActuelle) ? '<li class="active"><a href="">'.$i.'</a></li>' :
						'<li><a href="'.RACINE_SITE.'admin/membres.php?page='.$i .$recupereGet.'">'.$i. '</a></li>' ?>	
					<?php
					endfor;
					?>
				</ul>
			</nav>
		</div>
		<!-- fin pagination -->
		<!-- affichage des membres -->
		<table class="table table-stripped table-bordered table-condensed">
			<tr>
				<th class="text-center inverse">Id membre</th>
				<th class="text-center inverse">Pseudo</th>
				<th class="text-center inverse">Nom</th>
				<th class="text-center inverse">Prenom</th>
				<th class="text-center inverse">Email</th>
				<th class="text-center inverse">Civilité</th>
				<th class="text-center inverse">Statut</th>
				<th class="text-center inverse">Date enregistrement</th>
				<th class="text-center inverse">Actions</th>
			</tr>
		<?php
		foreach($membres as $membre) :
		?>
			<tr class="text-center">
				<td><?= $membre['id_membre'] ?></td>
				<td><?= $membre['pseudo'] ?></td>
				<td><?= $membre['nom'] ?></td>
				<td><?= $membre['prenom'] ?></td>
				<td><?= $membre['email'] ?></td>
				<td><?= $membre['civilite'] == 'm' ? 'Homme' : 'Femme' ?></td>
				<td><?= $membre['statut'] == 1 ? 'Admin' : 'Utilisateur' ?></td>
				<td><?= date("d/m/Y H:i", (strtotime($membre['date_enregistrement']))); ?></td>
				<td>
				<a href="<?= RACINE_SITE . 'admin/membres.php?id='. $membre['id_membre'] ?>" class="btn btn-primary btn-sm" title="Modifier les infos du membre"><span class="glyphicon glyphicon-edit" title="Modifier les infos du membre"></span></a>&nbsp;<a href="<?= RACINE_SITE . 'admin/membre-delete.php?id='. $membre['id_membre'] ?>" class="btn btn-danger btn-sm" title="Supprimer le membre"><span class="glyphicon glyphicon-trash" title="Supprimer le membre"></span></a></td>
			</tr>
		<?php
		endforeach;
		?>
		</table>

	</fieldset>
	<!-- Modification membre -->
	<?php
	if ($testId) :
	?>
	<fieldset>
		<legend>Modifier les infos membres</legend>
		<form method="POST">
			<div class="row">
				<div class="col-md-6">

					<div class="form-group <?= getErrorClass('pseudo', $errors); ?>">
						<label for="pseudo" class="control-label">Pseudo</label>
						<input type="text" class="form-control" name="pseudo" placeholder="Votre pseudo" value="<?= $pseudo; ?>" />
						<?= displayErrorMsg('pseudo', $errors) ?>
					</div>

					<div class="form-group <?= getErrorClass('nom', $errors); ?>">
							<label for="nom" class="control-label">Nom</label>
							<input type="text" class="form-control" name="nom" placeholder="Votre nom" value="<?= $nom; ?>" />
						<?= displayErrorMsg('nom', $errors) ?>
					</div>

					<div class="form-group <?= getErrorClass('prenom', $errors); ?>">
						<label for="prenom" class="control-label">Prenom</label>
						<input type="text" class="form-control" name="prenom" placeholder="Votre prénom" value="<?= $prenom; ?>" />
						<?= displayErrorMsg('prenom', $errors) ?>
					</div>

				</div>

				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('email', $errors); ?>">
						<label for="email" class="control-label">Email</label>
						<input type="email" class="form-control" name="email" placeholder="Votre email" value="<?= $email; ?>" />
						<?= displayErrorMsg('email', $errors) ?>
					</div>

					<div class="form-group <?= getErrorClass('civilite', $errors); ?>">
						<label for="civilite" class="control-label">Civilité</label>
						<select name="civilite" class="form-control">
							<option value="m" <?= ($civilite === 'm') ? 'selected' : ''; ?>>Homme</option>
							<option value="f" <?= ($civilite === 'f') ? 'selected' : ''; ?>>Femme</option>
						</select>
					</div>

					<div class="form-group <?= getErrorClass('statut', $errors); ?>">
						<label for="statut" class="control-label">Statut</label>
						<select name="statut" class="form-control">
							<option value="0" <?= ($statut == 0) ? ' selected' : ''; ?>>Utilisateur</option>
							<option value="1" <?= ($statut == 1) ? ' selected' : ''; ?>>Admin</option>
						</select>
					</div>

					<div class="form-group pull-right">
						<button class="btn btn-primary">Modifier</button>
					</div>
				</div>
			</div>
		</form>
	</fieldset>
	<?php
	endif;
	?>
<!-- fin container-->
</div>

<?php 
require '../layout/footer-admin.php';
?>