<?php
require '../include/init.php';
adminSecurity();
/**
* Charger les CSS & JS pour activer le TimePicker dans
* le  header.php et footer.php
*/
$loadTimePicker = '';
require '../layout/header.php';

/**
* Gestion des erreurs
*/
$errors = [];

$date_depart = $date_arrivee = $id_salle = $prix = '';

if (!empty($_POST)){
	sanitizePost();
	extract($_POST);

	if (empty($date_arrivee)){
		$errors['date_arrivee'] = 'Vous n\'avez pas indiqué une date d\'arrivée.';
	} elseif (strtotime($date_arrivee) == false){
		$errors['date_arrivee'] = 'Format de date invalide.';
	} elseif (strtotime($date_arrivee) > strtotime($date_depart)){
		$errors['date_arrivee'] = 'La date d\'arrivée ne peut pas être postérieure à la date de départ.';
	} elseif (strtotime($date_arrivee) < time()){
		$errors['date_arrivee'] = 'La date d\'arrivée ne peut pas être antérieure à la date du jour.';
	}

	if (empty($date_depart)){
		$errors['date_depart'] = 'Vous n\'avez pas indiqué une date de départ.';
	} elseif (strtotime($date_depart) == false){
		$errors['date_depart'] = 'Format de date invalide.';
	}

	if (strtotime($date_depart) == strtotime($date_arrivee)){
		$errors['date_depart'] = 'La date de départ ne peut pas être la même que la date d\'arrivée.';
	} elseif (strtotime($date_depart) < strtotime($date_arrivee)) {
		$errors['date_depart'] = 'La date de départ ne peut pas être antérieure à la date d\'arrivée.';
	} elseif (strtotime($date_depart) < time()){
		$errors['date_depart'] = 'La date de départ ne peut pas être antérieure à la date du jour.';
	}

	if (empty($id_salle)){
		$errors['id_salle'] = 'Vous n\'avez pas selectionné une salle.';
	}

	if (empty($prix)){
		$errors['prix'] = 'Vous n\'avez pas entré de prix.';
	} elseif (!ctype_digit($prix)){
		$errors['prix'] = 'Le prix ne doit contenir que des chiffres.';
	}

	if (empty($errors)){
		/**
		 * On vérifie s'il existe déjà un produit aux mêmes dates que celui
		 * qu'on tente d'ajouter à la base
		 */
		$query = "SELECT * FROM produit" 
					. " WHERE id_salle = :id_salle"
					. " AND ((date_arrivee BETWEEN :date_arrivee AND :date_depart)"
					. " OR (date_depart BETWEEN :date_arrivee AND :date_depart))"
					;
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':id_salle', $id_salle, PDO::PARAM_INT);
		$stmt->bindParam(':date_arrivee', $date_arrivee, PDO::PARAM_STR);
		$stmt->bindParam(':date_depart', $date_depart, PDO::PARAM_STR);			
		$stmt->execute();
		$doublons = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (!empty($doublons)){
			setFlashMessage('Il existe déjà un produit aux dates séléctionnées pour cette salle.', 'error');
		} else {
		/**
		 * Si pas de doublons, ajout dans la base
		 */
		$etat= 'libre';
		$query = "INSERT INTO produit (id_salle, date_arrivee, date_depart, prix, etat)"
					. " VALUES (:id_salle, :date_arrivee, :date_depart, :prix, :etat)"
					;
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':id_salle', $id_salle, PDO::PARAM_INT);
		$stmt->bindParam(':date_arrivee', $date_arrivee, PDO::PARAM_STR);
		$stmt->bindParam(':date_depart', $date_depart, PDO::PARAM_STR);
		$stmt->bindParam(':prix', $prix, PDO::PARAM_INT);
		$stmt->bindParam(':etat', $etat, PDO::PARAM_STR);
		$stmt->execute();

		setFlashMessage('Nouveau produit ajouté.');
		}
	}
}

/**
* Requêtes des produits pour peupler l'affichage du tableau
*/
$query = "SELECT p.*, s.titre, s.photo "
					. "FROM produit p "
					. "JOIN salle s ON p.id_salle = s.id_salle "
					;
/*
* On crée un array comprenant les noms des colonnes et la table à laquelle
* elles correspondent en SQL avant de l'envoyer à la fonction getOrderBy()
* qui s'en servira pour ajouter à la query les infos nécessaires au
* classement du tableau. 
* Le tableau associatif n'est nécessaire que parce que certains tableaux
* affichent des résultats provenant de plusieurs tables SQL différentes.
* On pourrait remplacer le nom complet de la table par son initiale mais
* je trouve cette écriture plus lisible.
* Temps passé sur cette fonction : 2 - 3 - 4 heures.
* Satisfaction personnelle vis à vis de cette fonction : OFF THE CHARTS
*/ 
$results = ['id_produit' => 'produit', 
			'date_arrivee' => 'produit', 
			'date_depart' => 'produit', 
			'id_salle' => 'produit', 
			'prix' => 'produit', 
			'etat' => 'produit'];
$query .= getOrderBy($results);

/**
* Pagination - intercallé avant l'execution de la requête afin de 
* gerer l'ajout de LIMIT dans $query
* 
*  - Nombre de produits à afficher par page
*/
if (isset($_GET['parPage'])){
	$parPage = $_GET['parPage'];
} else {
	$parPage = 5;
}

/**
 * La fonction pagination renvoie un tableau qu'on sépare en trois variables
 * avec list().
 */
list($nbPages, $pageActuelle, $debutPagination) = pagination($pdo, 'produit', $parPage);

if (isset($_GET['page'])){
		$query .=  "LIMIT " . $debutPagination . ", ". $parPage;
} else {
		$query .= "LIMIT 0, " . $parPage;
}
$stmt = $pdo->query($query);
$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);

/**
 * Requête des salles pour peupler le <select> dans le formulaire
 * d'ajout d'un produit
 */
$stmt = $pdo->query("SELECT * FROM salle");
$salles = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>

<div class="container">
<?= displayFlashMessage() ?>

	<fieldset id="table">
		<legend>Tout les produits</legend>
		<div class="row">
		<!-- classer l'affichage des résultats -->
		<div class="col-md-7">
		<form class="form-inline pb-10" method="get">
			<div class="form-group">
				<label class="control-label" for="classer">Classer par :</label>
				<select class="form-control" name="classer">
					<!-- on foreach $results qui contient les noms des colonnes du tableau -->
					<?php
					foreach ($results as $id => $table) :

					?>
					<option value="<?= $id ?>" <?= isset($_GET['classer']) && $_GET['classer'] == $id ? 'selected' : ''?> ><?= $id ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" name="ordre">
					<option value="ASC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'ASC' ? 'selected' : '' ?>>ascendant</option>
					<option value="DESC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'DESC' ? 'selected' : '' ?>>descendant</option>
				</select>
			</div>
			<div class="form-group">
				<label> Résultats par page :</label>
				<select class="form-control" name="parPage">
					<?php
					for($i=5; $i<20; $i+=5) :
					?>
						<option value="<?= $i ?>" <?= isset($_GET['parPage']) && $_GET['parPage'] == $i ? 'selected' : '' ?>><?= $i ?></option>
					<?php
					endfor;
					?>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Classer</button>
		</form>
		</div>
		<!-- pagination -->
		<div class="col-md-5 pull-right">
			<nav id="pagination" aria-label="Pagination">

				<ul class="pagination no-margin pb-10 pull-right">
					<?php
					for ($i = 1; $i <= $nbPages; $i++):
						$recupereGet = '';
						$recupereGet .= isset($_GET['classer']) ? '&classer=' . $_GET['classer'] : '';
						$recupereGet .= isset($_GET['ordre']) ? '&ordre='. $_GET['ordre'] : '';
						$recupereGet .= isset($_GET['parPage']) ? '&parPage='. $_GET['parPage'] : '';
					?>
						<?= ($i == $pageActuelle) ? '<li class="active"><a href="">'.$i.'</a></li>' :
						'<li><a href="'.RACINE_SITE.'admin/produits.php?page='.$i .$recupereGet.'">'.$i. '</a></li>' ?>	
					<?php
					endfor;
					?>
				</ul>
			</nav>
		</div>
		<!-- fin pagination -->
		<!-- affichage des resultats -->
		<table class="table table-bordered table-condensed table-striped">
			<tr>
				<th class="text-center inverse">Id produit</th>
				<th class="text-center inverse">Date d'arrivée</th>
				<th class="text-center inverse">Date de départ</th>
				<th class="text-center inverse">Id salle</th>
				<th class="text-center inverse">Prix</th>
				<th class="text-center inverse">Etat</th>
				<th class="text-center inverse">Actions</th>
			</tr>
		<?php
		foreach ($produits as $produit) :
		?>
			<tr class="text-center">
				<td><?= $produit['id_produit']; ?></td>
				<td><?= date("d/m/Y H:i", (strtotime($produit['date_arrivee']))); ?></td>
				<td><?= date("d/m/Y H:i", (strtotime($produit['date_depart']))); ?></td>
				<td>
				<?php 
					echo $produit['id_salle'] . ' - Salle ' . $produit['titre'];
					echo '<br><img class="thumbnail-table" src="' . PHOTO_SITE . $produit['photo'] . '">';
				?>
					
				</td>
				<td><?= $produit['prix'] . ' €'; ?></td>
				<td><?= $produit['etat']; ?></td>
				<td>
				<a href="<?= RACINE_SITE . 'produit-view.php?id='. $produit['id_produit'] ?>" class="btn btn-primary btn-sm" title="Voir la fiche produit"><span class="glyphicon glyphicon-search" title="Voir la fiche produit"></span></a>&nbsp;
				<a href="<?= RACINE_SITE . 'admin/produit-edit.php?id='. $produit['id_produit'] ?>" class="btn btn-primary btn-sm" title="Editer le produit"><span class="glyphicon glyphicon-edit" title="Editer le produit"></span></a>
				<?php 
				if ($produit['etat'] == 'libre') :
				?>
				&nbsp;<a href="<?= RACINE_SITE . 'admin/produit-delete.php?id='. $produit['id_produit'] ?>" class="btn btn-danger btn-sm" title="Supprimer le produit"><span class="glyphicon glyphicon-trash" title="Supprimer la produit"></span></a>&nbsp;
				<?php
				endif;
				?>
			</td>
			</tr>
		<?php
		endforeach;
		?>
		</table>


	</fieldset>

	<!-- Formulaire - Ajout de produit -->
	<fieldset>
		<legend>Ajouter un nouveau produit</legend>
		<form method="post">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('date_arrivee', $errors) ?>">
						<label for="date_arrivee" class="control-label">Date d'arrivée</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="text" id="date_arrivee" class="form-control" name="date_arrivee" placeholder="YYYY-mm-dd hh:mm:ss" value="<?= $date_arrivee ?>"/>
							
							</div>
							<div><?= displayErrorMsg('date_arrivee', $errors) ?></div>
							
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('id_salle', $errors) ?>">
						<label for="id_salle" class="control-label">Salle</label>
							<select name="id_salle" class="form-control">
							<option value="">Selectionnez une salle : </option>
							<?php
							foreach ($salles as $salle) :
								$concat = $salle['id_salle'] . ' - Salle ' . $salle['titre']  . ' - ' . $salle['adresse'] . ', ' . $salle['cp'] . ', ' . $salle['ville'] . ' - ' . $salle['capacite'] . ' pers';
							?>
								<option value="<?= $salle['id_salle'] ?>" <?= ($id_salle == $salle['id_salle']) ? 'selected' : '' ?>><?= $concat ?></option>
							<?php
							endforeach;
							?>
							</select>
							<div><?= displayErrorMsg('id_salle', $errors) ?></div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('date_depart', $errors) ?>">
						<label for="date_depart" class="control-label">Date de départ</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							<input type="text" id="date_depart" class="form-control" name="date_depart" placeholder="YYYY-mm-dd hh:mm:ss" value="<?= $date_depart ?>"/>
							</div>
							<div><?= displayErrorMsg('date_depart', $errors) ?></div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group <?= getErrorClass('prix', $errors) ?>">
						<label for="prix" class="control-label">Tarif</label>
							<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-eur"></span></span>
							<input type="text" class="form-control" name="prix" placeholder="Prix en euros" value="<?= $prix ?>"/>
							</div>
							<div><?= displayErrorMsg('prix', $errors) ?></div>
					</div>
				</div>
				<div class="col-md-12 text-right">
				<button class="btn btn-primary">Enregistrer</button>
				</div>

			</div>
		</form>
	</fieldset>

</div>
<?php 
require '../layout/footer-admin.php';
?>