<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';

// Dashboard

$stmt = $pdo->query("SELECT COUNT(*) FROM salle");
$nbSalles = $stmt->fetchColumn();

$stmt = $pdo->query("SELECT COUNT(*) FROM produit WHERE etat = 'libre' AND date_arrivee > NOW()");
$nbProduits = $stmt->fetchColumn();

$stmt = $pdo->query("SELECT COUNT(*) FROM commande");
$nbCommandes = $stmt->fetchColumn();

$stmt = $pdo->query("SELECT COUNT(*) FROM avis");
$nbAvis = $stmt->fetchColumn();

// Statistiques

$query = "SELECT s.titre, AVG(a.note) as note FROM salle s "
		."JOIN avis a ON s.id_salle = a.id_salle "
		."GROUP BY s.titre ORDER BY note DESC LIMIT 5 "
		;
$stmt = $pdo->query($query);
$meilleuresNotes = $stmt->fetchAll(PDO::FETCH_ASSOC);

$query = "SELECT s.titre, COUNT(p.id_salle) as nbCommande FROM commande c "
		."JOIN produit p ON c.id_produit = p.id_produit "
		."JOIN salle s ON p.id_salle = s.id_salle "
		."GROUP BY s.titre ORDER BY nbCommande DESC LIMIT 5"
		;
$stmt = $pdo->query($query);
$plusCommandes = $stmt->fetchAll(PDO::FETCH_ASSOC);

$query = "SELECT m.nom, m.prenom, COUNT(c.id_membre) as achats FROM membre m "
		."JOIN commande c ON m.id_membre = c.id_membre "
		."GROUP BY m.id_membre ORDER BY achats DESC LIMIT 5"
		;
$stmt = $pdo->query($query);
$plusAchats = $stmt->fetchAll(PDO::FETCH_ASSOC);

$query = "SELECT m.nom, m.prenom, SUM(p.prix) as total FROM commande c "
		."JOIN produit p ON c.id_produit = p.id_produit "
		."JOIN membre m  ON c.id_membre = m.id_membre "
		."GROUP BY m.id_membre ORDER BY total DESC LIMIT 5"
		;
$stmt = $pdo->query($query);
$maxAchats = $stmt->fetchAll(PDO::FETCH_ASSOC);

require '../layout/nav.php';
?>

<div class="container">
	<?= displayFlashMessage() ?>
	<legend>Dashboard admin</legend>
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h3 class="panel-title"><?= $nbSalles ?> salles</h3>
				</div>
				<div class="panel-body">
				<p><a class="text-success" href="<?= RACINE_SITE; ?>admin/salles.php">Gérer les salles. <span class="glyphicon glyphicon-circle-arrow-right pull-right"></span></a></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title"><?= $nbProduits ?> produits réservables</h3>
				</div>
				<div class="panel-body">
				<p><a href="<?= RACINE_SITE; ?>admin/produits.php">Gérer les produits. <span class="glyphicon glyphicon-circle-arrow-right pull-right"></span></a></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?= $nbCommandes ?> commandes</h3>
				</div>
				<div class="panel-body">
				<p><a class="text-danger" href="<?= RACINE_SITE; ?>admin/commandes.php">Gérer les commandes. <span class="glyphicon glyphicon-circle-arrow-right pull-right"></span></a></p>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-warning">
				<div class="panel-heading">
					<h3 class="panel-title"><?= $nbAvis ?> avis</h3>
				</div>
				<div class="panel-body">
				<p><a class="text-warning" href="<?= RACINE_SITE; ?>admin/avis.php">Gérer les avis. <span class="glyphicon glyphicon-circle-arrow-right pull-right"></span></a></p>
				</div>
			</div>
		</div>
	</div>
	<legend>Statistiques</legend>
	<div class="row">
		<div class="col-md-3">
			<p><strong>Salles les mieux notées</strong></p>
			<div class="list-group">
			<?php
			foreach($meilleuresNotes as $note) :
			?>
				<a class="list-group-item" href="#">
				<span class="badge"><?= number_format($note['note'], 2, ',', ' '); ?></span>
				<?= $note['titre'] ?>
				</a>
			<?php
			endforeach;
			?>
			</div>
		</div>
		<div class="col-md-3">
			<p><strong>Salles les plus commandées</strong></p>
			<div class="list-group">
			<?php
			foreach($plusCommandes as $cmd) :
			?>
				<a class="list-group-item" href="#">
				<span class="badge"><?= $cmd['nbCommande'] ?></span>
				<?= $cmd['titre'] ?>
				</a>
			<?php
			endforeach;
			?>
			</div>
		</div>
		<div class="col-md-3">
			<p><strong>Membres achetant le plus de produits</strong></p>
			<div class="list-group">
			<?php
			foreach($plusAchats as $achats) :
			?>
				<a class="list-group-item" href="#">
				<span class="badge"><?= $achats['achats'] ?></span>
				<?= $achats['prenom'] .' '.$achats['nom'] ?>
				</a>
			<?php
			endforeach;
			?>
			</div>
		</div>
		<div class="col-md-3">
			<p><strong>Membres dépensant le plus (total)</strong></p>
			<div class="list-group">
			<?php
			foreach($maxAchats as $achats) :
			?>
				<a class="list-group-item" href="#">
				<span class="badge"><?= $achats['total'] ?> €</span>
				<?= $achats['prenom'] .' '.$achats['nom'] ?>
				</a>
			<?php
			endforeach;
			?>
			</div>
		</div>
	</div>
</div>

<?php 
require '../layout/footer-admin.php';
?>

