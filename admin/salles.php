<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';

$errors = [];
$titre = $description = $capacite = $categorie = $pays = $ville = $adresse = $cp = '';

if (!empty($_POST)){
	sanitizePost();
	extract($_POST);

	if (empty($titre)){
		$errors['titre'] = 'Un titre est nécessaire.';
	} elseif (strlen($titre) > 200){
		$errors['titre'] = 'Wow, du calme. Pas plus de 200 caractères dans un nom de salle.';
	}

	if (empty($description)){
		$errors['description'] = 'Une description de la salle est nécessaire.';
	}

	if (empty($capacite)){
		$errors['capacite'] = 'Une salle doit avoir une capacité.';
	} elseif (!ctype_digit($capacite)){
		$errors['capacite'] = 'La capacité d\'une salle doit être exprimée avec des chiffres.';
	}

	if (empty($adresse)){
		$errors['adresse'] = 'Une adresse de la salle est nécessaire.';
	}

	if (empty($cp)){
		$errors['cp'] = "Vous n'avez pas entré de code postal.";
	} elseif (!ctype_digit($_POST['cp']) || strlen($_POST['cp']) !=5){
		$errors['cp'] = "Le code postal est invalide.";
	}

	if (empty($_FILES['photo']['tmp_name'])){
		$errors['photo'] = 'Une photo de la salle est obligatoire.';
	} else {
		if ($_FILES['photo']['error'] != 0){
			$errors['photo'] = 'Problème d\'upload de l\'image.'; 
		}
		if ($_FILES['photo']['size'] > 150000){
			$errors['photo'] = 'Le poids de la photo de ne doit pas excéder 150 Ko.';
		}
		$allowedMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];
		if (!in_array($_FILES['photo']['type'], $allowedMimeTypes)){
			$errors['photo'] = 'Format de fichier incorrect. Acceptés : .jpg / .png / .gif';
		}
	}
/**
 * Tests théoriquements inutiles, des options étant séléctionnées par défaut, mais on ne sait 
 * jamais, les utilisateurs peuvent être débrouillards et réussir l'impossible.
 */
	if (empty($categorie) || empty($pays) || empty($ville)){
		$errors['categorie'] = 'Je ne sais pas comment mais vous avez réussi à ne pas choisir une catégorie. Merci de ne pas refaire ça :).';
	}

/**
 * Si on ne trouve pas d'erreurs dans le formulaire,
 * on upload la photo sur le serveur, prépare la query 
 * puis insère les données dans la base
 */
	if (empty($errors)){
		$extension = substr($_FILES['photo']['name'], strrpos($_FILES['photo']['name'], '.'));
		$photo = md5(time()) . $extension;
		move_uploaded_file($_FILES['photo']['tmp_name'], PHOTO_SITE . $photo);


		$query = "INSERT INTO salle (titre, description, photo, pays, ville, adresse, cp, capacite, categorie) VALUES (:titre, :description, :photo, :pays, :ville, :adresse, :cp, :capacite, :categorie)";
		$stmt = $pdo->prepare($query);
		$stmt->bindParam(':titre', $titre, PDO::PARAM_STR);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);
		$stmt->bindParam(':photo', $photo, PDO::PARAM_STR);
		$stmt->bindParam(':pays', $pays, PDO::PARAM_STR);
		$stmt->bindParam(':ville', $ville, PDO::PARAM_STR);
		$stmt->bindParam(':adresse', $adresse, PDO::PARAM_STR);
		$stmt->bindParam(':cp', $cp, PDO::PARAM_INT);
		$stmt->bindParam(':capacite', $capacite, PDO::PARAM_INT);
		$stmt->bindParam(':categorie', $categorie, PDO::PARAM_STR);
		$stmt->execute();
		setFlashMessage('Nouvelle salle ajoutée avec succès.');
	} else {
		setFlashMessage('Erreur(s) présente(s) dans le formulaire.', 'error');
	}
}
/**
* Peupler le tableau contenant toutes les salles
*/
$query = "SELECT * FROM salle s ";
$results = ['id_salle' => 'salle', 
			'titre' => 'salle', 
			'description' => 'salle',  
			'photo' => 'salle', 
			'pays' => 'salle', 
			'ville' => 'salle', 
			'adresse' => 'salle', 
			'cp' => 'salle', 
			'capacite' => 'salle', 
			'categorie' => 'salle'];
$query .= getOrderBy($results);

/**
* Pagination - intercallé avant l'execution de la requête afin de 
* gerer l'ajout de LIMIT dans $query
 */
if (isset($_GET['parPage'])){ 
	$parPage = $_GET['parPage'];
} else {
	$parPage = 5; // nombres de résultats que l'on souhaite afficher par page
}
list($nbPages, $pageActuelle, $debutPagination) = pagination($pdo, 'salle', $parPage);

if (isset($_GET['page'])){
		$query .=  " LIMIT " . $debutPagination . ", ". $parPage;
} else {
		$query .= " LIMIT 0, " . $parPage;
}
/**
 * Fin De la pagination, on execute la requête ensuite
 */
$stmt = $pdo->query($query);
$salles = $stmt->fetchAll(PDO::FETCH_ASSOC); 

/**
 *  Si vous lisez ce message, vous avez gagné un (1) café à l'agora de l'IFOCOP Paris XI.
 *  Offre valide uniquement pour Julien Anest, s'il peut dire ou se trouvait ce message.
 */
require '../layout/nav.php';
?>

<div class="container">
<?= displayFlashMessage() ?>
<fieldset id="table">
	<legend>Toutes les salles</legend>
		<div class="row">
		<!-- classer l'affichage des résultats -->
		<div class="col-md-7">
		<form class="form-inline pb-10" method="get">
			<div class="form-group">
				<label class="control-label" for="classer">Classer par :</label>
				<select class="form-control" name="classer">
					<!-- on foreach $results qui contient les noms des colonnes du tableau -->
					<?php
					foreach ($results as $id => $table) :

					?>
					<option value="<?= $id ?>" <?= isset($_GET['classer']) && $_GET['classer'] == $id ? 'selected' : ''?> ><?= $id ?></option>
					<?php
					endforeach;
					?>
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" name="ordre">
					<option value="ASC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'ASC' ? 'selected' : '' ?>>ascendant</option>
					<option value="DESC" <?= isset($_GET['ordre']) && $_GET['ordre'] == 'DESC' ? 'selected' : '' ?>>descendant</option>
				</select>
			</div>
			<div class="form-group">
				<label> Résultats par page :</label>
				<select class="form-control" name="parPage">
					<?php
					for($i=5; $i<20; $i+=5) :
					?>
						<option value="<?= $i ?>" <?= isset($_GET['parPage']) && $_GET['parPage'] == $i ? 'selected' : '' ?>><?= $i ?></option>
					<?php
					endfor;
					?>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Classer</button>
		</form>
		</div>
		<!-- pagination -->
		<div class="col-md-5 pull-right">
			<nav id="pagination" aria-label="Pagination">

				<ul class="pagination no-margin pb-10 pull-right">
					<?php
					for ($i = 1; $i <= $nbPages; $i++):
						$recupereGet = '';
						$recupereGet .= isset($_GET['classer']) ? '&classer=' . $_GET['classer'] : '';
						$recupereGet .= isset($_GET['ordre']) ? '&ordre='. $_GET['ordre'] : '';
						$recupereGet .= isset($_GET['parPage']) ? '&parPage='. $_GET['parPage'] : '';
					?>
						<?= ($i == $pageActuelle) ? '<li class="active"><a href="">'.$i.'</a></li>' :
						'<li><a href="'.RACINE_SITE.'admin/salles.php?page='.$i .$recupereGet.'">'.$i. '</a></li>' ?>	
					<?php
					endfor;
					?>
				</ul>
			</nav>
		</div>
		<!-- fin pagination -->
		<!-- Affichage des salles -->
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th class="text-center inverse">Id_salle</th>
			<th class="text-center inverse">Titre</th>
			<th class="text-center inverse">Description</th>
			<th class="text-center inverse">Photo</th>
			<th class="text-center inverse">Pays</th>
			<th class="text-center inverse">Ville</th>
			<th class="text-center inverse">Adresse</th>
			<th class="text-center inverse">CP</th>
			<th class="text-center inverse">Capacite</th>
			<th class="text-center inverse">Categories</th>
			<th class="text-center inverse">Actions</th>
		</tr>
	<?php
	foreach ($salles as $salle) :
	?>
		<tr class="text-center">
			<td><?= $salle['id_salle']; ?></td>
			<td><?= $salle['titre']; ?></td>
			<td><?= substr($salle['description'], 0, 40) . ' ..'; ?></td>
			<td><img class="thumbnail-table" src="<?= PHOTO_SITE . $salle['photo']; ?>"></td>
			<td><?= $salle['pays']; ?></td>
			<td><?= $salle['ville']; ?></td>
			<td><?= $salle['adresse']; ?></td>
			<td><?= $salle['cp']; ?></td>
			<td><?= $salle['capacite']; ?></td>
			<td><?= $salle['categorie']; ?></td>
			<td>

				<a href="<?= RACINE_SITE . 'admin/salle-edit.php?id='. $salle['id_salle'] ?>" class="btn btn-primary btn-sm" title="Editer les informations"><span class="glyphicon glyphicon-edit"></span></a>
				<a href="<?= RACINE_SITE . 'admin/salle-delete.php?id='. $salle['id_salle'] ?>" class="btn btn-danger btn-sm" title="Supprimer la salle"><span class="glyphicon glyphicon-trash" ></span></a>
			</td>
		</tr>
	<?php
	endforeach;
	?>
	</table>
</fieldset>

<!-- Formulaire ajout de salle-->
<fieldset id="formulaire">
	<legend>Ajouter une salle</legend>
	<form method="post" enctype="multipart/form-data">
		<div class="row">
		<!-- Colonne gauche-->
			<div class="col-md-6">
				<!-- titre -->
				<div class="form-group <?= getErrorClass('titre', $errors) ?>">
					<label class="control-label" for="titre">Titre</label>
					<div>
						<input type="text" class="form-control" name="titre" placeholder="Titre de la salle" value="<?= $titre ?>">
						<?= displayErrorMsg('titre', $errors) ?> 
					</div>
				</div>
				<!-- description -->
				<div class="form-group <?= getErrorClass('description', $errors) ?>">
					<label class="control-label" for="description">Description</label>
					<div>
					<textarea class="form-control" name="description" placeholder="Description de la salle"><?= $description ?></textarea>
					<?= displayErrorMsg('description', $errors) ?> 
					</div>
				</div>
				<!-- photo-->
			 	<div class="form-group <?= getErrorClass('photo', $errors) ?>">
			 		<label class="control-label" for="photo">Photo</label>
			 		<div>
			 			<input type="file" class="form-control" name="photo">
			 			<?= displayErrorMsg('photo', $errors) ?> 
			 		</div>
				</div>
				<!-- capacité -->
				 <div class="form-group <?= getErrorClass('capacite', $errors) ?>">
				 	<label class="control-label" for="capacite">Capacité</label>
					<div>
						<input type="text" class="form-control" name="capacite" placeholder="Capacité de la salle" value="<?= $capacite ?>">
						<?= displayErrorMsg('capacite', $errors) ?> 
				 	</div>
				 </div>
				<!-- categorie -->
			 	<div class="form-group <?= getErrorClass('categorie', $errors) ?>">
				 	<label class="control-label" for="categorie">Catégorie</label>
					<div>
						<select class="form-control" name="categorie">
							<option value="reunion" <?= ($categorie == 'reunion') ? 'selected="selected"' : '' ?>>Réunion</option>
							<option value="bureau" <?= ($categorie == 'bureau') ? 'selected="selected"' : '' ?>>Bureau</option>
							<option value="formation" <?= ($categorie == 'formation') ? 'selected="selected"' : '' ?>>Formation</option>
						</select>
						<?= displayErrorMsg('categorie', $errors) ?>
			 		</div>
			 	</div>				 						
			</div>

		<!-- Colonne droite -->
			<div class="col-md-6">
			 	<div class="form-group <?= getErrorClass('pays', $errors) ?>">
			 		<label class="control-label" for="pays">Pays</label>
			 		<div>
						<select class="form-control" name="pays">
							<option value="France">France</option>
						</select>
						<?= displayErrorMsg('pays', $errors) ?>
			 		</div>
			 	</div>	
			 	<div class="form-group <?= getErrorClass('ville', $errors) ?>">
			 		<label class="control-label" for="ville">Ville</label>
			 		<div>
						<select class="form-control" name="ville">
							<option value="Paris" <?= ($ville == 'Paris') ? 'selected="selected"' : '' ?>>Paris</option>
							<option value="Lyon" <?= ($ville == 'Lyon') ? 'selected="selected"' : '' ?>>Lyon</option>
							<option value="Marseille" <?= ($ville == 'Marseille') ? 'selected="selected"' : '' ?>>Marseille</option>
						</select>
						<?= displayErrorMsg('ville', $errors) ?>
			 		</div>
			 	</div>
			 	<div class="form-group <?= getErrorClass('adresse', $errors) ?>">
			 		<label class="control-label" for="adresse">Adresse</label>
			 		<div>
			 			<textarea class="form-control" name="adresse" placeholder="Adresse de la salle"><?= $adresse ?></textarea>
			 			<?= displayErrorMsg('adresse', $errors) ?> 		 		
			 		</div>
			 	</div>
				<div class="form-group <?= getErrorClass('cp', $errors) ?>">
				 	<label class="control-label" for="cp">Code postal</label>
				 	<div>
				 		<input type="text" class="form-control" name="cp" placeholder="Code postal de la salle" value="<?= $cp ?>">
				 		<?= displayErrorMsg('cp', $errors) ?> 
			 		</div>
			 	</div>
			 	<div class="form-group text-right">
			 	<button type="submit" class="btn btn-primary">Enregistrer</button>
			 	</div>
			</div>
			<!-- Fin colonnes-->
		</div>
	</form>
</fieldset>
</div>
<?php 
require '../layout/footer-admin.php';
?>