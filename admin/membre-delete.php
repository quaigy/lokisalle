<?php
require '../include/init.php';
adminSecurity();
require '../layout/header.php';
/**
 * On tente de récupérer le membre correspondant à l'id $_GET['id']
 */
if (!empty($_GET) ){
	if ($_GET['id'] == $_SESSION['user_info']['id_membre']){
		redirectMsg('Vous ne pouvez pas supprimer votre propre compte.', 'error', 'membres.php');
	} else {
		$query = "SELECT * "
				."FROM membre "
				."WHERE id_membre =" . $pdo->quote($_GET['id']);
		$stmt = $pdo->query($query);
		$membre = $stmt->fetch();
	}
	/**
	 * Si le résultat est nul, on redirige
	 */
	if (!$membre) { 
		redirectMsg('Membre inconnu.', 'error', 'membres.php');
	/**
	 * Sinon, on efface le membre de la base
	 */
	} else {
		if (!empty($_POST) && isset($_POST['effacer-membre'])){
			$query = "DELETE FROM membre WHERE id_membre = " . $pdo->quote($_GET['id']);
			$pdo->exec($query);
			$message = 'Le membre #' . $membre['id_membre'] .  ' a été supprimé.';
			redirectMsg($message, '','membres.php');
		}
	}
} else {
	$message = 'Vous n\'avez pas atteint cette page d\'une manière autorisée. Petit malin !';
	redirectMsg($message, 'error', 'index.php');
}

require '../layout/nav.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 thumbnail text-center">
			<p class="text-danger">Vous êtes sur le point d'effacer le membre :</p>

			<p class="text-danger"><strong>Membre #<?= $membre['id_membre'] ?> - Pseudo #<?= $membre['pseudo'] ?> <br> <?= $membre['prenom'] . ' ' . $membre['nom'] ?>. </strong></p>
			<p class="text-danger"><strong>Cette action est définitive.</strong></p>
			<p>Êtes-vous sur de vouloir continuer ?</p>
			
			<a href="commandes.php"><button type="button" class="btn btn-success">Retour en arrière</button></a>

			<br><br>
			<p>Ou bien</p>

			<form method="post">
			<button type="submit" class="btn btn-danger" name="effacer-membre">Effacer le membre</button>
			</form>
			<br>
		</div>
	</div>
</div>
<?php 
require '../layout/footer-admin.php';
?>